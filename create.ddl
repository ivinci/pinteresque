drop table if exists topics;
create table topics (

);

drop table if exists texts;
create table texts (
id             serial primary key,
direction      text default 'out', /* in or out, for future use     */
type           text,    /* question or statement aka ? or !         */
intent         text,    /* intent-name as mentioned in model        */
model          text,    /* name of model used                       */
content        text,    /* spoken text, may contain clue-references */
positivematch  text,    /* regexp for positive emphasis             */
negativematch  text,    /* regexp for negative emphasis             */
cluetoinduce   text,    /* this text induces the other to solve clue-name */
active         boolean default true,
modifier       text     /* changes the use of this text             */
);

drop table if exists persons;
create table persons (
id        serial primary key,
starttime timestamp with time zone default now(),/* beginning of life*/
endtime   timestamp with time zone, /* end of life             */
isrobot   boolean default false,    /* is this person a robot? */
model     text                      /* null for any model, otherwise name */
);

drop table if exists clues;
create table clues (
id        serial primary key,
person    int references persons(id), /* in case of a clue-instance */
name      text,                 /* name of clue (is an intent slot) */
value     text,                 /* its value                        */
priority  int,                  /* 0-100, 0 is top prio             */
pit       timestamp with time zone,/* moment of value assignment    */
model     text                  /* the model, can be null for any model */
postags   text,                 /* blank spaced list of POStags to keep in text of value of clue */
);
/*create unique index un_clues on clues( person, name, model );*/
alter table clues add constraint clues_un UNIQUE (name,person,model);

drop table if exists names;
create table names (
id             serial primary key,
name           text,
sex            text
);

drop table if exists trail;
create table trail (
id             serial primary key,
pit            timestamp with time zone default now(),
intentname     text,
persona        int references persons(id),
person         int references persons(id),
pintext        int references texts(id),
intent         jsonb,
output         text
);

create or replace function toptexts( theperson  int,
                                     thepersona int,
                                     theclue    text,
                                     themodel   text,
                                     theintent  text
                                   ) returns
    table(id int, direction text, type text, intent text, model text, content text,
          positivematch text, negativematch text, cluetoinduce text,
          p int, active boolean, modifier text, ordering int ) as
$$

   select t.id, t.direction, t.type, t.intent, t.model, t.content,
          t.positivematch, t.negativematch, t.cluetoinduce,
          coalesce( (select priority
                       from clues
                      where     name = t.cluetoinduce
                            and person = theperson), 0 ) as p,
          t.active, t.modifier, 1 as ordering
     from texts t
    where     (t.intent ilike theintent and t.model = themodel)
          and t.active
          and coalesce(modifier,'') != '@'
          and coalesce(modifier,'') != '%'
          and coalesce(modifier,'') != '~'
          and t.direction = 'out'
          and (not
               (t.id in (select distinct coalesce(pintext,0) from trail
                         where persona = thepersona and person = theperson)
                )
               or coalesce( t.modifier, '' )
                  in ('*', '&') )
          and not
               (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                   in (select name from clues
                        where person = theperson and
                              (model is null or model = themodel) and
                              not value is null))
      UNION
   select t.id, t.direction, t.type, t.intent, t.model, t.content,
          t.positivematch, t.negativematch, t.cluetoinduce, 5 as p,
          t.active, t.modifier, 3 as ordering
     from texts t
    where t.intent = (select intent
                        from texts it
                       where     it.active
                             and coalesce( it.modifier, '' ) != '@'
                             and it.direction = 'in'
                             and it.model = themodel
                             and it.cluetoinduce = theclue
                       group by intent
                       order by count( intent ) DESC LIMIT 1)
          and t.model = themodel
          and t.active
          and t.direction = 'out'
          and (not
               (t.id in (select distinct coalesce(pintext,0) from trail
                         where persona = thepersona and person = theperson)
                )
               or coalesce( t.modifier, '' )
                  in ('*', '&') )
      UNION
   select t.id, t.direction, t.type, t.intent, t.model, t.content,
          t.positivematch, t.negativematch, t.cluetoinduce,
          coalesce( (select priority
                       from clues
                      where     name = t.cluetoinduce
                            and person = theperson), 0 ) as p,
          t.active, t.modifier, 4 as ordering
     from texts t
    where     (not t.intent ilike theintent and t.model = themodel)
          and t.active
          and coalesce(modifier,'') != '@'
          and coalesce(modifier,'') != '%'
          and coalesce(modifier,'') != '~'
          and t.direction = 'out'
          and (not
               (t.id in (select distinct coalesce(pintext,0) from trail
                         where persona = thepersona and person = theperson)
                )
               or coalesce( t.modifier, '' )
                  in ('*', '&') )
          and not
               (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                   in (select name from clues
                        where person = theperson and
                              (model is null or model = themodel) and
                              not value is null))
      UNION
   select t.id, t.direction, t.type, t.intent, t.model, t.content,
          t.positivematch, t.negativematch, t.cluetoinduce,
          coalesce( (select priority
                       from clues
                      where     name = t.cluetoinduce
                            and person = theperson), 0 ) as p,
          t.active, t.modifier, 5 as ordering
     from texts t
    where     (not t.intent ilike theintent and t.model = themodel)
          and t.active
          and coalesce(modifier,'') = '%'
          and coalesce(modifier,'') != '~'
          and t.direction = 'out'
          and (not
               (t.id in (select distinct coalesce(pintext,0) from trail
                         where persona = thepersona and person = theperson)
                )
               or coalesce( t.modifier, '' )
                  in ('%') )
          and not
               (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                   in (select name from clues
                        where person = theperson and
                              (model is null or model = themodel) and
                              not value is null))
    order by ordering, p
   
$$
language 'sql';
