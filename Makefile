.EXPORT_ALL_VARIABLES:
.SUFFIXES=.arm
all: pinteresque.arm livecaption.arm texttospeech.arm pin-sidechannel.arm  mouse-sidechannel.arm
pinteresque: pinteresque.org
	go get $@
	go build $@
livecaption: pinteresque.org
	go get $@
	go build $@
texttospeech: pinteresque.org
	go get $@
	go build $@
pin-sidechannel: pinteresque.org
	go get $@
	go build $@
mouse-sidechannel: pinteresque.org
	go get $@
	go build $@

pinteresque.arm livecaption.arm texttospeech.arm pin-sidechannel.arm mouse-sidechannel.arm: pinteresque livecaption texttospeech pin-sidechannel mouse-sidechannel
	GOOS=linux GOARCH=arm go build -o $@ $(basename $@)

installremote: pinteresque.arm texttospeech.arm pin-sidechannel.arm mouse-sidechannel.arm bins
	rsync -cp pinteresque.arm pin@ivi.local:bot/pinteresque
	rsync -cp texttospeech.arm pin@ivi.local:bot/texttospeech
	rsync -cp pin-sidechannel.arm pin@ivi.local:bot/pin-sidechannel
	rsync -cp mouse-sidechannel.arm pin@ivi.local:bot/mouse-sidechannel
	rsync -cp nlu-pipe.py pin@ivi.local:bot/

bins: models
	rsync -cp bin/anno bin/asr bin/nlu bin/pinnames bin/pintexts2db bin/pintrain bin/renew bin/ir-bot bin/selector bin/sidechannel bin/tts bin/preport bin/startpinteresque bin/todisplay pin@ivi.local:bot/bin/
	rsync -cp setenv.example pin@ivi.local:bot/

models:
	rsync -cp ir-bot.md ir-prs.md selector.md preport.in.txt sorry.md pin@ivi.local:bot/
	rsync -cp selomat.png preport.in.txt preport-all.in.txt webreport.in.html nlu_config.yml pin@ivi.local:bot/

publish:
	convert -resize x64 selomat-logo.png favicon.ico
	rsync -g --groupmap "*:www-data" --chmod=u+rw,g+r,o-rwx planning.html pinteresque.html ar.html sa@pinteresque.nl:/var/www/pinteresque
	rsync -g --groupmap "*:www-data" --chmod=u+rw,g+r,o-rwx *.png favicon.ico sa@pinteresque.nl:/var/www/pinteresque/
	rsync -g --groupmap "*:www-data" --chmod=u+rw,g+r,o-rwx top.html sa@pinteresque.nl:/var/www/pinteresque/index.html
	rsync -r -g --groupmap "*:www-data" --chmod=u+rw,g+r,o-rwx nsvp naturalis sorry-smelt vng sa@pinteresque.nl:/var/www/pinteresque/
