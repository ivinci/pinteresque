## intent:begin
+ @waarvoor zou jij nog excuses willen ontvangen?>her.naam

## intent:naam
- ik ben [patty](naam)
- ik heet [gerard](naam)
- ik ben [maria](naam)
- hallo ik ben [patty](naam)
- hallo ik heet [gerard](naam)
- hallo ik ben [maria](naam)
- hoi ik ben [joost](naam)
- hoi ik heet [marijke](naam)
- hoi ik ben [mohammed](naam)
- ik heet [doryn](naam)
- mijn naam is [doryn](naam)
- [dorian](naam)
- [moniek](naam)
- [daphne](naam)
- [myrthe](naam)
- [rozemarijn](naam)
- [geurt](naam) heet ik
- [moniek](naam) is mijn naam
- mijn naam is [myrthe](naam) met een m
- ik heet [patricia](naam)
- mijn naam is [eduardo](naam)
- ik heet [maria](naam)
- ik heet [mark](naam)
- hallo ik heet [karin](naam)
- hallo met [jolanda](naam)
- als [jaap](naam)
- hallo [piet](naam) hier
+ Goed, fijn dat je hier bent.\
  Vertel eens, waarvoor zou je nog excuses willen ontvangen?>her.waarvoor

## intent:waarvoor
- al die keren dat mensen je [overhalen om nog even te blijven](waarvoor) ja dat
- van een [goede vriendin](wie) met wie ik [backpackte](wat) die me een [half uur uitschold](waarvoor)
- ik zou graag excuses willen ontvangen voor al mij [dateleed](waarvoor) die lopen vaak vrij slecht af
- toen hoorde ik ineens helemaal [niets meer](waarvoor) van hem
- nou mijn moeder had me [niet uitgenodigd op haar verjaardag](waarvoor)
- ik ben [uitgescholden](waarvoor)
- een winkel heeft de [spullen niet geleverd](waarvoor)
- ik ben [ontslagen](waarvoor)
- voor een [te late terugbetaling](waarvoor)
- voor de [botsing](waarvoor)
- ik werd [aangereden](waarvoor)
- iemand [bekraste mijn auto](waarvoor)
- ik zou nog wel excuses willen ontvangen van [tessa](wie) omdat zij dat [podium](waar) de [lak niet droog](waarvoor) was
- voor de mevrouw die van de week zei dat ik [niet op de stoep mocht fietsen](waarvoor) terwijl ik minder ruimte inneem
- ik kreeg een [klap in mijn gezicht](waarvoor)
- iemand heeft me [dronken gevoerd](waarvoor)
- voor de [botsing](waarvoor)
- eh, van [mijn baas](wie) omdat opeens soort van heldere hemel dat er [geen ruimte meer is](waarvoor) voor ontwikkeling
- Eh, gewoon. We hadden vlak daarvoor een [beetje ruzie gehad](waarvoor), dus misschien dat het daardoor komt?
- Voor het [missen van de trein](waarvoor).
+ Wat vervelend zeg! Kun je me daar nog iets meer over vertellen?>her.meer1
+ Kun je mij vertellen wat dit met jou deed?>her.aangedaan

## intent:aangedaan
- al die keren dat voelt gewoon
- het [geeft een gevoel](aangedaan) dat ik me [schuldig voel](gevoel) [om weg te gaan](wat)
- dat ik een beetje [uit mijn kracht wordt gehaald](aangedaan)
- een beetje [pissig](gevoel) en ik kan ook mezelf voor de kop slaan omdat ik [onvoorzichtig ben](rol)
- ik werd er een [beetje boos](gevoel) van want ik [voelde me onbegrepen](aangedaan)
- ik denk dat ik er [onzeker](aangedaan) van werd
- ik voelde me [heel verdrietig](aangedaan) want het was heel gezellig
- ik schrok ervan en was er door [van slag](aangedaan)
- mijn toekomstperspectief was aan het [wankelen](aangedaan) gebracht
- Ja, veel [gedoe](aangedaan), geld kwijt en heel veel verdriet
- Waar moet ik beginnen [veel pijn](aangedaan)
- Wat ik zeg, een [hoop ellende](aangedaan)
+ En als je nou echt eerlijk tegen jezelf bent, wat voor gevoel gaf dit jou?>her.gevoel

## intent:gevoel
- voor mezelf voelt het [een beetje slap](gevoel)
- ja [boos](gevoel) [licht pissig](gevoel) eigenlijk [geirriteerd](gevoel)
- dat ik mij [onbegrepen voelde](gevoel)
- een gevoel van [onzekerheid en minderwaardigheid](gevoel) denk ik
- [verdrietig onzekerheid](gevoel) want je ben zo oninteressant
- dat wil ik liever niet zeggen eh [verraden](gevoel)
- iets als een [klap in het gezicht](gevoel)
- ik vind het [stom](gevoel)
- ik voelde me [niet gezien](gevoel)
- ja ik was eerst [boos](gevoel) daarna verdrietig
- (nijdig)[gevoel] werd ik
- flink (pissig)[gevoel] natuurlijk
- ja eerst [geschrokken](gevoel) daarna boos
- Je voelt je [machteloos](gevoel) en boos tegelijk
+ Dat snap ik wel. Kun je nog voor mij herhalen wie dat precies deed?>her.wie

## intent:wie
- [heel veel mensen](wie)
- [tessa](wie)
- dat was een [onbekende mevrouw](wie) met een hondje uit de buurt
- een [jongen](wie) die ik via tinder heb leren kennen
- mijn [moeder](wie) dus
- het was [Jaap](wie) die het deed
- De [Bijenkorf](wie) was ervoor verantwoordelijk
- mijn [neef](wie) deed het
- het was [de winkel](wie)
- nou dat eh was [mijn broer](wie)
- natuurlijk [Agnes](wie) die doet dat soort dingen
- de [portier](wie) denk ik
- ja dat was [mediamarkt](wie)
+ Waarom heeft diegene dat gedaan, denk je?>her.waarom

## intent:waarom
- omdat ze het [gezellig](waarom) vinden dat [je blijft](wat)
- ik denk niet dat [ze](wie) het [expres](waarom) heeft gedaan
- waarschijnlijk omdat ze wel vaker [fietsers op de stoep](waarom) ziet
- een kwestie van [jaloezie](waarom) en de onzekerheid
- ik weet dat ie [niet super lekker in zijn vel zat](waarom) dus misschien komt het daar door
- het was gewoon [slechte timing](waarom).
- nou ik denk de vorm waarin dat was hoe het gebeurde een [nieuw inzicht](waarom) was
- ik denk niet om pijn te doen maar het is ook meer een [zakelijke overweging](waarom) was
- ik denk dat ze het gewoon [vergeten](waarom) was eigenlijk
- pure [haat](waarom) is het
- ik heb [geen idee](waarom)
- weet ik veel
- dat was [onkunde](waarom) verwacht ik
+ Hmm, ja. Dat zou kunnen. Wat had deze persoon anders kunnen doen?>her.anders

## intent:anders
- volgende keer [beter opletten](anders)
- gewoon [niet](anders) tot in den treure [overhalen](wat)
- op het moment dat ik zeg niet [dat dat dan genoeg](anders) is.
- meerdere dingen ze had even kunnen zeggen dat ik [moest opletten](anders)
+ En wat was jouw aandeel hierin?>her.rol

## intent:rol
- ik laat dan ook [merken](wat) dat ik het [moeilijk vind om weg](rol) te gaan
- ik had gewoon gelijk toen het nat was en had [super voorzichtig](rol) kunnen zijn
- mijn rol was dat ik haar [in de weg zat](rol)
- mijn rol was dat ik uiteindelijk de gene was die het [accepteerde](rol) en verder ging
- dat vind ik een [moeilijke vraag](rol)
- misschien heb ik niet duidelijk gemaakt dat ik [serieus](rol) was
- het ging [over mij](rol)
- ik [hoorde het aan](rol)
- ik had haar misschien wel [kunnen bellen](rol) nadat we ruzie hebben gehad
- ik had iets beter [kunnen doorlopen](rol) natuurlijk
- het lag helemaal [niet aan mij](rol)
- ik speelde [geen](rol) rol
+ %Goed, dat waren mijn vragen. Ik denk dat ik genoeg weet om jou te kunnen\
  helpen. Ik ga nu aan de slag om het perfecte excuus voor jou te genereren.

## intent:genoeg
- stop er maar mee
- ik heb er genoeg van
- zo is het wel genoeg
- ik ben er wel klaar mee
+ %Goed, dat waren mijn vragen. Ik denk dat ik genoeg weet om jou te kunnen\
  helpen. Ik ga nu aan de slag om het perfecte excuus voor jou te genereren.

## intent:herhalen
- watte
- [nog eens](herhaling)
- kun je dat [herhalen](herhaling)
- kun je dat [nog eens zeggen](herhaling)
- wat zei je
- wat zeg je
- ik heb het niet goed [verstaan](herhaling)
- ik heb het niet helemaal [gevolgd](herhaling), zou je dat kunnen herhalen
- zou je dat in [andere woorden](herhaling) uit kunnen leggen
- wat was dat [laatste](herhaling)
+ (me.lastoutput)

## intent:meer-meer
- dat iedereen er alles aan doet om [over je grenzen](meer1) heen te gaan
- ik vind het lastig om mijn [eigen grenzen](meer2) aan te geven en dat andere mensen die dat weten ook een beetje misbruik van maken
- ze had het als het goed is de dag ervoor al [gelakt](wat) en [ik dacht dat het droog was](meer1)
- ze zei [je begrijpt er helemaal niets van](meer1) terwijl ik juist wel over haar had nagedacht
- ik denk dat het gewoon [heel moeilijk was](meer1) en dat ze [dominanter is](waarom) dan ik
- nou eigenlijk was het een [extern bureau](meer1) dat was ingehuurd
- eh ja [wat moet ik](meer1) er nog meer over zeggen
- uhm
- dat soort dingen zorgen voor een [vreemde interactie](meer1) met iemand
- dat weet ik [niet](meer1)
- ja [begrijp je](meer1) wat ik bedoel
- ik had het naar [tessa](wie) geappt
- ik zou graag een sorry willen oh ja zo had ik het nog niet bekeken
- ja dat heeft me altijd [dwars gezeten](meer2) en toen ik terug kwam heb ik wel eventjes mijn tijd moeten hebben
- hij was ineens [verdwenen](meer2) en ja en ik voelde me te [beschaamd](gevoel)
- nou en ik had wel gehoopt dat hij er [op was teruggekomen](meer2)
- dat hij er [gevolg aan](meer2) zou geven
- het was [duidelijk](meer2) dat het zo ging
- ik had wel [gehoopt](meer2) dat hij daar op terug zou komen
+ *uhm, ja. Ga door.>her.meer2
+ *uhm, ja. Ga door.>her.meer1

## intent:byebye
+ ~bedankt en tot ziens.

## clue:naam
p 0
v Moniek
t SPEC LID N
## clue:waarvoor
p 10
v uitgescholden
t N WW ADJ
## clue:meer1
p 20
v wat en zo
## clue:meer2
p 30
v wat en zo
## clue:aangedaan
p 40
v pijn
t N ADJ WW
## clue:gevoel
p 50
v Geschrokken
t ADJ N WW
## clue:wie
p 60
v Gerard
t SPEC LID ADJ N TW
## clue:waarom
p 70
v vergeten
t N WW
## clue:anders
p 65
v mondhouden
t WW ADJ
## clue:rol
p 80
v opletten
t WW ADJ N
## clue:waar
p 90
v stoep
t N
## clue:wat
p 91
v gelakt
t N WW
## clue:geslacht
p 100
v v
## clue:lastoutput
p 100
v uhm
## clue:lastinput
p 100
v uhm
