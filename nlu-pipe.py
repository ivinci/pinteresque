import sys
import os
import signal
from rasa_nlu.model import Interpreter
import json

def doNLU( model ):
    interpreter = Interpreter.load( "models/" + model )
    line = sys.stdin.readline()
    while line:
        if line.startswith( "#!" ): # it's a command
           command = line[ len( "#!" ): ].rstrip()
           if command.startswith( "model " ):
               newmodel = command[ len( "model " ): ]
               if newmodel != model:
                  model = newmodel
                  interpreter = Interpreter.load( "models/" + model )
           try:
              print( command ) # repeat for further down the pipe-line
           except IOError: # handle output being gone
              break
        else:
           try:
              result = interpreter.parse( line.rstrip() )
              print( json.dumps(result) )
           except IOError: # handle output being gone
              break
        try:
           sys.stdout.flush() # try to provoke an IOError in case stdout closed
           line = sys.stdin.readline()
        except IOError: # handle input being gone
           break

model = "kappersklant"
if len( sys.argv ) == 2:
      model = sys.argv[ 1 ]
if not os.path.isdir( "models/" + model ):
      print( "Given parameter is not a model: ", model )
      sys.exit( -1 )
doNLU( model )
