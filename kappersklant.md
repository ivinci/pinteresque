## intent:groet
- hallo.
- hi.
- hai.
- goedemorgen.
- goedenavond.
+ goeiedag.

## intent:naam
- hoi ik ben [patty](naam)
- hoi ik heet [gerard](naam)
- hai ik ben [maria](naam)
- hi ik ben [moniek](naam)
- hi ik heet [patricia](naam)
- mijn naam is [eduardo](naam)
- [goedemorgen](dagdeel), ik heet [maria](naam)
- [goedemiddag](dagdeel), ik heet [maria](naam)
- [goedenavond](dagdeel), ik heet [karin](naam)
+ mooie naam, (her.naam). Ik heet (naam).
+ hoe heet jij? >her.naam
+ wat is je naam? >her.naam

## intent:knippen
- hoe wil je dat ik je knip?
- hoe wil je geknipt worden?
- hoe wil je je haar hebben?
+ kun je me ongeveer hetzelfde knippen als nu? maar dan korter. >her.kent knipwens
+ zo'n beetje (haarlengte). >her.kent knipwens

## intent:knippenoordekking
- wilt je de oren [gedekt](dekking) houden?
- oren [gedekt](dekking)?
- oren [vrij](dekking)?
- wilt je de oren [vrij](dekking) houden?
- wilt je je oren [vrij](dekking)?
- wilt je de oren [gedekt](dekking)?
+ oren graag (dekking), maar niet te opvallend. >her.kent knipwens

## intent:knippenlengte
- wil je [lang](haarlengte) haar?
- wil je [kort](haarlengte) haar?
- wil je [halflang](haarlengte) haar?
+ (haarlengte), graag, of ietsje langer, mag ook >her.kent knipwens

## intent:knipopdrachtbevestiging
- [ok](kent knipwens), [halflang](haarlengte) en oren [vrij](dekking) [dus](kent knipwens)
- [ok](kent knipwens), oren [vrij](dekking) en [halflang](haarlengte) [dus](kent knipwens)
- [lang](haarlengte) en oren [gedekt](dekking) [dus](kent knipwens)
- oren [vrij](dekking) en [lang](haarlengte) [dus](kent knipwens)
- oren [vrij](dekking) en [kort](haarlengte) [dus](kent knipwens)
- je wil [dus](kent knipwens) [lang](haarlengte) en oren [gedekt](dekking)
- je wil [dus](kent knipwens) oren [gedekt](dekking) en [lang](haarlengte)
+ doe maar, inderdaad.
+ helemaal goed hoor.

## intent:kappersprodukten
- heb je nog iets nodig als shampoo of gel
- je weet dat we haarverzorgingsartikelen van hoge kwaliteit verkopen
+ heb je iets tegen van dat droge haar?
+ twee flessen shampoo graag.

## intent:reistijd
- moet je van ver komen
- had je lang reizen
- hoe ver, enkele reis
+ een uur enkele reis.

## intent:hobby
- ik [teken en schilder](hobby) af en toe
- ik [naai kleren](hobby) voor vrienden en kennissen
- ik doe aan [dansen](hobby)
+ heb jij nog een hobby? >her.hobby
+ knippen is vast je lust en je leven, maar doe je er nog iets naast? >her.hobby
+ ik ben klaarover op school

## intent:sport
- ik speel [voetbal](sport)
- ik speel [hockey](sport)
- ik speel [volleybal](sport)
- ik doe aan [cricket](sport)
- [cricket](sport)
- [la crosse](sport)
- doe jij aan sport
- welke sport doe jij
+ ik speel (me.sport)
+ wat voor sport doe jij? >her.sport
+ ik (me.sport)
+ doe je ook aan sport? >her.sport

## intent:verkeer
- was het gemakkelijk te vinden
- was het makkelijk te vinden
- had je [een beetje](verkeersdruk) file vanmorgen
- had je [veel](verkeersdruk) file vanmorgen
- had je [veel](verkeersdruk) file
- was er [veel](verkeersdruk) file
+ ik had (file) file; had jij last van het verkeer vandaag? >her.file
+ ik had (file) file hoor

## intent:leeftijd
- [30](leeftijd)
- ik ben [42](leeftijd)
- [18](leeftijd) jaar
+ ik ben (me.leeftijd)
+ mag ik vragen hoe oud je bent? >her.leeftijd

## intent:pinty-internal
- robot stop.
- robot quit.
+ ok, doe ik

## intent: de-weg-kwijt
- snap je het niet
@ hm
+ sorry, ik begrijp je niet

## clue:naam
p 0
v piet
## clue:geslacht
p 10
v m
## clue:leeftijd
p 20
v 24
## clue:werk
p 30
v receptionist
## clue:studie
p 50
v geen
## clue:dekking
p 20
v vrij
## clue:haarlengte
p 20
v kort, maar gedekt
## clue:kinderen
p 40
v 1
## clue:file
p 40
v veel
## clue:sport
p 30
v voetbal

## clue:naam
p 0
v christina
## clue:geslacht
p 10
v v
## clue:leeftijd
p 20
v 21
## clue:werk
p 30
v geen
## clue:studie
p 50
v communicatiewetenschappen
## clue:dekking
p 20
v bedekt
## clue:haarlengte
p 20
v halflang
## clue:kinderen
p 40
v 0
## clue:file
p 40
v weinig
## clue:sport
p 30
v hockey
