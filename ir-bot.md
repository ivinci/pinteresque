;;; bot zou ook naam werkgever moeten noemen, praten is pluggen
## intent:begin
+ @Hoi, wat fijn dat je er bent en welkom op ons hoofdkantoor. ik hoop dat\
  je het een beetje hebt kunnen vinden?>her.verkeersissues

## intent:verkeerok
- ja hoor dat is [gelukt](verkeersissues)
- dat ging [prima](verkeersissues)
- ja hoor [prima](verkeersissues)
- zeker dat ging [vanzelf](verkeersissues)
- ja hoor [geen probleem](verkeersissues)
- ja hoor [geen file](verkeersissues)
- dat ging [goed](verkeersissues)
- [ja hoor](verkeersissues) dat ging wel goed
- [ja](verkeersissues)
+ Mooi zo. Je komt hier solliciteren op positie (me.vacature), allereerst\
  wil ik je wat praktische vragen stellen. daarna volgen wat uitgebreidere\
  vragen. Hoe spreek je je voornaam precies uit?>her.naam

## intent:verkeernietok
- nou ik vond het toch [lastig](verkeersissues)
- ik was even de weg [kwijt](verkeersissues) maar vond hem toch weer
- ik had wat [file](verkeersissues)
- [nou](verkeersissues) ik was wel de weg kwijt in het gebouw
- dat ging [niet helemaal](verkeersissues) goed
+ Nou, gelukkig dat je er toch bent gekomen. Je komt hier solliciteren op\
  positie (me.vacature), allereerst wil ik je wat praktische vragen stellen.\
  daarna volgen wat uitgebreidere vragen. Hoe spreek je je\
  voornaam uit?>her.naam

;;; veel manieren om een naam over te brengen, voorlopig alleen voornamen
## intent:naam
- ik ben [patty](naam)
- ik heet [gerard](naam)
- ik ben [maria](naam)
- [moniek](naam)
- [geurt](naam) heet ik
- ik heet [patricia](naam)
- mijn naam is [eduardo](naam)
- ik heet [maria](naam)
- ik heet [mark](naam)
- ik heet [karin](naam)
- met [jolanda](naam)
- als [jaap](naam)
+ Ok, (her.naam). En wat is je leeftijd?>her.leeftijd

;;; het is mij niet duidelijk of en hoe ik (leeftijd) moet opsplitsen in (leeftijdpos) en (leeftijdneg)
## intent:leeftijdpos
- ik ben [32](leeftijd)
- [21](leeftijd) jaar ben ik
- mijn leeftijd is [19](leeftijd)
- [jong](leeftijd) genoeg zou ik zeggen
- [23](leeftijd) jaar oud
- [28](leeftijd)
+ Ok, dat is goed om te weten, ik kon het niet terugvinden op je CV. Mijn\
  naam is (me.naam), (me.afdeling), met mij zul je te maken krijgen mocht\
  je hier in dienst komen, en ik ben ook het eerste aanspreekpunt binnen\
  het sollicitatietraject. Ben je klaar voor de volgende vraag?>her.wilgesprek

## intent:leeftijdneg
- [45](leeftijd) is mijn leeftijd
- [dat](leeftijd) zeg ik liever niet
- [45](leeftijd)
- [laten we zeggen](leeftijd) boven de 50
- [61](leeftijd) jaar
- inmiddels ben ik [50](leeftijd)
- [jong genoeg](leeftijd) zou ik zeggen
+ Ok, dat is goed om te weten, ik kon het niet terugvinden op je CV. Mijn\
  naam is (me.naam), (me.afdeling), met mij zul je te maken krijgen mocht\
  je hier in dienst komen, en ik ben ook het eerste aanspreekpunt binnen\
  het sollicitatietraject. Ben je klaar voor de volgende\
  vraag?>her.wilgesprek

## intent:leeftijdneg
- ik weet niet waarom [leeftijd](leeftijd) relevant zou zijn
- waarom wil je mijn [leeftijd](leeftijd) weten
+ Ook goed. Ik kon het niet terugvinden op je CV. Mijn naam is (me.naam),\
  (me.afdeling), met mij zul je te maken krijgen mocht je hier in dienst\
  komen, en ik ben ook het eerste aanspreekpunt binnen het\
  sollicitatietraject. Ben je klaar voor de volgende vraag?>her.wilgesprek

;;; heb hier een tweede vraag aan toegevoegd.
## intent:klaarvoor
- [ja](wilgesprek) ik ben er klaar voor
- [ja](wilgesprek) hoor
- [ja](wilgesprek) hoor kom maar op
- [zeker](wilgesprek)
- [goed](wilgesprek)
- [OK](wilgesprek)
+ OK, vertel eens over een situatie waarin er een hoge werkdruk was en\
  je achter liep op schema?>her.situatie1
+ Beschrijf een situatie in het verleden waarin jouw sterke communicatie\
  skills het verschil maakte in een project of opdracht>her.situatie1

;;; veel meer intents hiervoor, eentje gaat niet werken!
;;; denk dat de tweede vraag niet opgelost gaat worden met de clues in (situatie1)
## intent:klaarvoorniet
- [nee](wilgesprek) nog niet ik had nog een vraag
- [nee](wilgesprek) kan ik nog iets vragen
- kan ik [nog](wilgesprek) iets vragen
+ Zullen we vragen tot het einde bewaren? Vertel eens over een situatie\
  waarin er een hoge werkdruk was en je achter liep op schema?>her.situatie1
+ Zullen we vragen tot het einde bewaren? Kun je me vertellen over een\
   situatie waarin je feedback kreeg die je niet had verwacht?>her.situatie1

## intent:drinkennogeens
- Nee, zou ik nog even wat [water](drinken) mogen halen
- Nee, zou ik [wat](drinken) mogen
+ Vooruit dan maar. Vertel eens over een situatie waarin er een hoge\
  werkdruk was en je achter liep op schema?>her.situatie1
+ Beschrijf een situatie in het verleden waarin jouw sterke communicatie\
  skills het verschil maakte in een project of opdracht>her.situatie1

## intent:antwoordsit1
- dat was in [2016](periode) toen we voor een tweede maal bezig waren\
  met een [reorganisatie](situatie1)
- in [2018](periode) hadden we een project lopen voor een klant waarvoor\
  veel [externe expertise](situatie1) nodig was, het was toen lastig\
  tijdig de juiste kennis aan te trekken.
- dat gebeurt eigenlijk [nooit](situatie1), dat ik achter liep op schema
- net voor de [zomer](periode) hadden we opeens een [personeelstekort](situatie1),\
  waardoor het team overuren moest draaien
- voor een opdrachtgever moesten we in één keer 100 [posities vullen](situatie1),\
  het was heel lastig om dat nog binnen dat kwartaal de juiste mensen aan te trekken.
+ Hoe heb je dat toen aangepakt?>her.taak1
+ Ok, dat is helder. Een andere vraag. Hoe zou je je eigen rol binnen een team\
  beschrijven?>her.competentie1

## intent:antwoordtk1
- ik heb het team [aangestuurd](taak1), zodat zij de juiste lijntjes\
  uit konden gooien
- ik heb veel [vacatures uitgezet](taak1) om zo onze outreach te vergroten
- ik heb [HR](taak1) gevraagd bij te springen
- ik heb het doorgespeeld aan [human recourses](taak1)
- onze [manager](taak1) heeft ingegrepen
- ik heb gekeken waar nog [capaciteit](taak1) beschikbaar was om de drukte\
  op te vangen
- ik ben heb in [samenspraak](taak1) met Human Resources gekeken of er toch\
  niet een mouw aan konden passen
- ik heb meerdere mensen [verantwoordelijk gemaakt](taak1) voor de sourcing\
  van personeel
- door de assessment te [automatiseren](taak1)
+ Waarom heb je ervoor gekozen het op die manier aan te pakken?>her.taak1waarom
+ En als je er over nadenkt, hoe zouden jouw collega's je\
  beschrijven?>her.competentie1


## intent:argumententaak1
- ik denk dat het belangrijk is [snel te schakelen](taak1waarom) in dit soort\
  situaties
- omdat [goed overleg](taak1waarom) zeer belangrijk is
- omdat [afstemmen](taak1waarom) zeer belangrijk is
- omdat ik denk dat dit in het team het [beste werkt](taak1waarom)
- omdat dit zorgt dat we voldoende mensen aantrekken zonder\
  meer [uren te schrijven](taak1waarom)
- omdat dit een [duurzame](taak1waarom) oplossing is voor dit probleem
- [zorgvuldige](taak1waarom) overwegingen speelden een rol
+ Wat was het resultaat?>her.resultaat1
+ En als je er over nadenkt, hoe zouden jouw collega's je\
  beschrijven?>her.competentie1

## intent:resultaat1noemen
- het resultaat was dat we binnen het kwartaal [weer bij](resultaat1) waren
- dat alles was op tijd [klaar](resultaat1)
- dat mensen meer [ontspannen](resultaat1) konden werken
- dat er aanzienkelijk [minder stress](resultaat1) was op de werkvloer
- dat we onze [targets](resultaat1) toch gehaald hebben
- dat we uiteindelijk toch een [tevreden](resultaat1) klant hadden
- dat we toch de juiste [selectie](resultaat1) hebben kunnen maken
- het [resultaat](resultaat1) was goed
+ Kun je me een voorbeeld geven van een lastig keuzeproces bij het aannemen\
  van een kandidaat?>her.keuzeproces
+ Ok, dat is helder. Een andere vraag. Hoe zou je je eigen werkwijze\
  beschrijven?>her.competentie2

## intent:keuzeprocesuitleggen
- we hadden toen een [geschikte](keuzeproces) kandidaat maar de manager vond\
  haar niet geschikt
- ik moest toen kiezen of ik iemand nog op de valreep wilde\
  [aannemen](keuzeproces)
- het gesprek was goed maar de [assessment](keuzeproces) liet hele andere\
  resultaten zien
- ik moest toen kiezen of ik iemand nog op de valreep wilde\
  [aanstellen](keuzeproces)
- ik had toen [te weinig](keuzeproces) kandidaten
- in feiten was er niet echt een [keuze](keuzeproces), ik moest het gewoon\
  zien te verkopen intern
+ Welke stappen heb je precies ondernomen?>her.stappen2
+ Ok, ik schrijf het even op. Hum, nog een andere vraag, hoe zou je je eigen\
  professionele houding beschrijven?>her.competentie2

;;; zou zoiets werken?
## intent:competentie1pos
- ik ben vrij [uitgesproken](competentie1)
- ik ben wat meer [extravert](competentie1)
- ik ben vrij [vriendelijk](competentie1)
- ik ben vrij [goedaardig](competentie1)
- ik ben vrij [assertief](competentie1)
- ik ben tamelijk [charismatisch](competentie1)
- ik ben tamelijk [enthousiast](competentie1)
- ik ben tamelijk [gezellig](competentie1)
- ik ben tamelijk [overtuigend](competentie1)
- ik ben tamelijk [zelfverzekerd](competentie1)
- ik ben tamelijk [spraakzaam](competentie1)
+ Ok, dat kunnen we wel gebruiken in deze functie, goed om te weten! hoe zou je\
  je eigen professionele houding beschrijven?>her.competentie2

## intent:competentie1neg
- ik ben vrij [introvert](competentie1)
- ik ben wat meer [tot mezelf](competentie1)
- ik ben vrij [voorzichtig](competentie1)
- ik ben vrij [gereserveerd](competentie1)
- ik ben vrij [alleen](competentie1)
- ik ben tamelijk [reflecterend](competentie1)
- ik ben tamelijk [gereserveerd](competentie1)
- ik ben tamelijk [gevoelig](competentie1)
- ik ben tamelijk [verlegen](competentie1)
+ Ok, dat hoeft natuurlijk geen probleem te zijn. hoe zou je je eigen professionele\
  houding beschrijven?>her.competentie2

## intent:competentie2pos
- ik ben [Actief](competentie2)
- ik ben [Ambitieus](competentie2)
- ik ben [Voorzichtig](competentie2)
- ik ben [creatief](competentie2)
- ik ben [Precies](competentie2)
- ik ben [nieuwsgierig](competentie2)
- ik ben [logisch](competentie2)
- ik ben [georganiseerd](competentie2)
- ik ben [perfect](competentie2)
- ik ben [perfectionistisch](competentie2)
- ik ben [nauwkeurig](competentie2)
+ %Dit spreekt van zelfkennis, dat is altijd goed om te horen. Ok, (her.naam),\
  dat waren mijn vragen. Hartelijk dank voor je tijd. Als je de hoorn zo ophangt\
  zie je meteen de uitkomsten van deze assessment. En je weet meteen of we je\
  uitnodigen voor een volgende ronde. Plak de resultaten zichtbaar op, zodat\
  onze human recruiters je kunnen herkennen.

## intent:competentie2neg
- ik ben [bezorgd](competentie2)
- ik ben [zorgeloos](competentie2)
- ik ben [ongeduldig](competentie2)
- ik ben [lui](competentie2)
- ik ben [stijf](competentie2)
- ik ben [ongefocust](competentie2)
- ik ben [sober](competentie2)
- ik ben [ongedisciplineerd](competentie2)
- ik ben [ongeconcentreerd](competentie2)
- ik ben [onstabiel](competentie2)
- ik ben [instabiel](competentie2)
+ Ah, dat is wel een heel eerlijk antwoord. Ok, (her.naam), dat waren mijn\
  vragen. Hartelijk dank voor je tijd. Als je de hoorn zo ophangt zie je na\
  15 seconden de uitkomsten van deze assessment. En je weet meteen of we je\
  uitnodigen voor een volgende ronde. Plak de resultaten zichtbaar op, zodat\
  onze human recruiters je kunnen herkennen.

## intent:noemtstappen2
- ik heb eerst gekeken of de [competenties](stappen2) van de kandidaat voldoende\
  aansloten, ook al waren er maar twee kandidaten, daarna heb ik het overlegd\
  met de team manager
- ik heb gekeken welk [budget](stappen2) beschikbaar was voor het vullen van\
  deze positie
- ik heb gekeken of we middels interne [training](stappen2) het gebrek aan\
  specifieke hard skills kunnen aanvullen met onze interne expertise
- ik heb [contact opgenomen](stappen2) met de juiste mensen
- ik heb [afgestemd](stappen2) met het team
- naar aanleiding van een aantal [star](stappen2) vragen konden we de kandidaten\
  beter vergelijken
- ik heb gekeken naar de beste [match](stappen2) tussen de functiebeschrijving en\
  de kandidaten
+ Waarom heb je voor deze aanpak gekozen?>her.waaromstappen

## intent:noemtwaaromstappen
- om zo [draagvlak](waaromstappen) te creëren voor mijn keuze
- omdat er [weinig keuze](waaromstappen) was, we moesten met deze kandidaat verder
- ik denk dat die het beste resultaat gaf voor de klantbeleving\
  van de [kandidaat](waaromstappen)
- om zo iedereen [mee te nemen](waaromstappen) in te proces
- om zo de [competenties](waaromstappen) gemakkelijker te kunnen testen
- om zo mijn keuze beter te kunnen [verantwoorden](waaromstappen)
- om zo mijn keuze beter te kunnen [verkopen](waaromstappen)
+ Wat was het resultaat?>her.resultaat2

## intent:noemtresultaat2
- dat we toch op tijd iemand hebben kunnen [aantrekken](resultaat2) om het\
  project te starten
- dat we [helaas](resultaat2 één van de deliverables niet op tijd konden opleveren
- dat we toch externe expertise moesten [inhuren](resultaat2), waardoor we\
  één van de deliverables niet op tijd konden opleveren
- het resultaat was dat we de kandidaat toch konden [aannemen](resultaat2), zij\
  werkt hier nog steeds
- het resultaat was [onenigheid](resultaat2) in het team
+ En, ben je hier tevreden over?>her.tevreden2

;;; als dit de afsluitende intent is, is deze dus heel belangrijk? kan die
;;; automatisch geintroduceerd worden worden op een of andere manier?
# intent:bentevreden
- ja zeker, ik denk dat we het beoogde [resultaat](tevreden2) wel hebben gehaald
- nou ik vind wel dat het [beter](tevreden2) had gekund
- nou nee, ik heb wel geleerd wat ik de volgende keer [anders](tevreden2) ga doen
- [zeker](tevreden2) tevreden
- [nee](tevreden2) niet zo erg
- [ja](tevreden2) erg tevreden
- [absoluut](tevreden2) zeker weten
+ %Ok, (her.naam), dat waren mijn vragen. Hartelijk dank voor je tijd. Als je de\
  hoorn zo ophangt zie je na 15 seconden de uitkomsten van deze assessment. En je\
  weet meteen of we je uitnodigen voor een volgende ronde. Plak de resultaten\
  zichtbaar op, zodat onze human recruiters je kunnen herkennen.

## intent:herhalen
- watte
- [nog eens](herhaling)
- kun je dat [herhalen](herhaling)
- kun je dat [nog eens zeggen](herhaling)
- wat zei je
- wat zeg je
- ik heb het niet goed [verstaan](herhaling)
- ik heb het niet helemaal [gevolgd](herhaling), zou je dat kunnen herhalen
- zou je dat in [andere woorden](herhaling) uit kunnen leggen
- wat was dat [laatste](herhaling)
+ (me.lastoutput)
+ ik herhaal (me.lastoutput)

## intent:de-weg-kwijt
- snap je het niet
- uhm
- begrijp je wat ik bedoel
+ *sorry, ik begrijp je niet, kun je dat op een andere manier omschrijven?
+ *hum, ok, kun je daar iets meer over vertellen?
+ *en wil je daar verder nog iets aan toevoegen?

## clue:naam
p 0
v Harald
## clue:geslacht
p 10
v m
## clue:leeftijd
p 5
v 31
## clue:functie
p 100
v medewerker pz
## clue:afdeling
p 100
v human resources
## clue:vacature
p 100
v inbound recruiter
## clue:verkeersissues
p 8
v ja
## clue:wilgesprek
p 10
v ja
## clue:werkgever
p 100
v Jansen en ko
## clue:drinken
p 40
v koffie
## clue:lastoutput
p 100
v uhm
## clue:lastinput
p 100
v uhm
## clue:situatie1
p 15
v placeholder
## clue:taak1
p 18
v placeholder
## clue:taak1waarom
p 20
v placeholder
## clue:resultaat1
p 24
v placeholder
## clue:keuzeproces
p 31
v placeholder
## clue:competentie1
p 28
v placeholder
## clue:competentie2
p 34
v placeholder
## clue:positiekandidaat
p 28
v placeholder
## clue:stappen2
p 34
v placeholder
## clue:waaromstappen
p 38
v placeholder
## clue:resultaat2
p 44
v placeholder
## clue:tevreden2
p 48
v placeholder

## clue:naam
p 0
v Patricia
## clue:geslacht
p 10
v v
## clue:leeftijd
p 5
v 31
## clue:functie
p 100
v medewerker pz
## clue:afdeling
p 100
v human resources
## clue:vacature
p 100
v inbound recruiter
## clue:verkeersissues
p 8
v ja
## clue:wilgesprek
p 10
v ja
## clue:werkgever
p 100
v Jansen en ko
## clue:drinken
p 40
v koffie
## clue:lastoutput
p 100
v uhm
## clue:lastinput
p 100
v uhm
## clue:situatie1
p 15
v placeholder
## clue:taak1
p 18
v placeholder
## clue:taak1waarom
p 20
v placeholder
## clue:resultaat1
p 24
v placeholder
## clue:keuzeproces
p 31
v placeholder
## clue:competentie1
p 28
v placeholder
## clue:competentie2
p 34
v placeholder
## clue:positiekandidaat
p 28
v placeholder
## clue:stappen2
p 34
v placeholder
## clue:waaromstappen
p 38
v placeholder
## clue:resultaat2
p 44
v placeholder
## clue:tevreden2
p 48
v placeholder
