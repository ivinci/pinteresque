## intent:begin
- [goedemorgen](dagdeel)
- [goedemiddag](dagdeel)
- [goedenavond](dagdeel)
+ @goedenmiddag, ik heet (me.naam), wat is je voornaam? >her.naam

## intent:naam
- ik ben [patty](naam)
- ik heet [gerard](naam)
- ik ben [maria](naam)
- [moniek](naam)
- ik heet [patricia](naam)
- mijn naam is [eduardo](naam)
- [goedemorgen](dagdeel), ik heet [maria](naam)
- [goedemiddag](dagdeel), ik heet [mark](naam)
- [goedenavond](dagdeel), ik heet [karin](naam)
+ mooie naam, (her.naam). Kon je het gemakkelijk vinden?>her.verkeersissues
+ hoe heet jij? >her.naam
+ wat is je naam? >her.naam

## intent:mefunctie
- wat is jouw [functie](functievraag)
- wat zijn jouw [werkzaamheden](functievraag)
- wat doe jij precies voor [werk](functievraag)
- wat doe jij voor [werk](functievraag)
+ ik ben (me.functie) en ik houd me voornamelijk bezig met (me.werkzaamheden)

## intent:verkeer
- ik had [weinig](verkeersissues) oponthoud
- ik had [file](verkeersissues), flink wat
- ik had wel wat [file](verkeersissues)
- er was [file](verkeersissues), maar niet veel
- [beetje](verkeersissues) file gehad, maar ging verder goed
- [beetje](verkeersissues) file gehad, maar het ging wel
- [weinig](verkeersissues) file gehad, maar het ging wel
- de [route](verkeersissues) was niet erg handig, maar het lukte me wel uiteindelijk
- navigatie zette me op het verkeerde been, maar het [lukte](verkeersissues) wel
- ja hoor, [geen](verkeersissues) probleem
- ja hoor, geen [file](verkeersissues)
- ja hoor
+ Fijn dat je er bent. Je komt voor de vacature (me.vacature). Ik ga je er wat vragen over stellen en jouw invulling ervan. >wilgesprek
+ Mooi dat je er bent. We gaan wat vragen stellen over de functie (me.vacature) en jouw invulling ervan. >wilgesprek

## intent:volgendevraag
- dat is [goed](wilgesprek)
- dat is [prima](wilgesprek)
- dat is [niet goed](wilgesprek)
- doe maar [niet](wilgesprek)
- doe maar [OK](wilgesprek)
- [kom maar](wilgesprek) op
- [sodemieter](wilgesprek) op
- begin [maar](wilgesprek)
- [OK](wilgesprek)
- [yep](wilgesprek)
- [prima](wilgesprek)
- liever [niet](wilgesprek)
+ Als (me.vacature) bij (me.werkgever) kom je af en toe in hectische situaties terecht. Kun je een voorbeeld geven van taken die jij als (me.vacature), in zo'n situatie hebt?>her.taak1
+ Bij (me.werkgever) is het af en toe rustig. Heb je een voorbeeld van soort werk die je, als (me.vacature), juist in zo'n periode doet?>her.taak1
+ Kun je een voorbeeld geven van een geboekt recent succes in je werk?>her.resultaat2
+ Heb je onlangs nog een groot succes beleefd? Kun je dat resultaat omschrijven?>her.resultaat2
+ mooie naam, (her.naam). Kon je het gemakkelijk vinden?>her.verkeersissues

; er zijn 4 vragen, met allemaal situatie<nummer>, taak<nummer>, actie<nummer> en resultaat<nummer> als clue
; vraag 1 loopt af als: taak1, actie1, situatie1, resultaat1
; vraag 2 loopt af als: resultaat2, actie2, taak2, situatie2
; vraag 3 loopt af als: taak3, resultaat3, actie3, situatie3
; vraag 4 loopt af als: actie4, resultaat4, situatie4, taak4
; dat doen we om te voorkomen dat meer dan eens dezelfde twee clues achterelkaar komen
; we onderscheiden steeds 4 intents: voor de leider, duiker, initiatiefnemer en de sociale collega
; in ons oordeel tellen we gewoon welke intents prevaleren en dat bepaalt of de functie geschikt is

## intent:dontuse
+ Ben je het steeds moeten presteren weleens moe?>her.resultaat1
+ Als (me.vacature) krijg je soms ook taken die buiten je functiebeschrijving liggen. Kun je daar een voorbeeld van geven en hoe je daarmee omging?>her.situatie2
+ Soms wordt je gevraagd wat te doen, waar je absoluut niet toe in staat bent. Kun je daarvan een voorbeeld geven?>her.situatie3
+ Er zal vast wel eens wat mislukt zijn in je werk, kun je een voorbeeld van geven van een actie die daartoe leidde?>her.actie4

## intent:situatie-leider
- een hectische omgeving is dat mensen onder [druk](situatie1) staan
- dat er veel [onrust](situatie1) is
- [onzerkerheid](situatie1) is er dan
+ Dat lijken me prima observaties; welke resultaten kun je dan nog boeken?>her.resultaat1
+ Interessant, niet de beste omstandigheid voor resultaten, of zie ik dat verkeerd?>her.resultaat1

## intent:situatie-duiker
- veel [drukte](situatie1) om me heen, mensen doorelkaar pratend
- mensen [doorelkaar](situatie1) pratend
- och, het maak [geen](situatie1) verschil
+ Lastig dan hè? Kun je dan nog tot resultaten komen?>her.resultaat1
+ Heb je daar dan wel of juist geen last van als het om resultaten gaat?>her.resultaat1

## intent:situatie-initiatiefnemer
- [druk](situatie1) of niet, dat maakt niet veel indruk op mij
- veel [onduidelijkheden](situatie1), maar zoals gezegd, dat maakt mij niet veel uit
+ Als dat niet veel uitmaakt, dan zul je wat betreft resultaat dus niet anders presteren, klopt dat?>her.resultaat1
+ Bij chaos vaart niemand wel, dat zal bij jou toch ook zo zijn; lever je dan nog wel?>her.resultaat1

## intent:situatie-sociaal
- heerlijke [drukte](situatie1) is leuker dan die [leegte](resultaat1) steeds
- het gaat om [interactie](situatie1), daar komt [produktiviteit](resultaat1) uit
+ Dat kan wel zijn, maar uiteindelijk is dat slecht voor je resultaten natuurlijk. Of niet?>her.resultaat1
+ Leidt dat niet erg af? Komt er dan nog iemand tot resultaten?>her.resultaat1

## intent:taken-leider
- mijn rol is dan die van [leider](taak1)
- ik neem dan de [lead](taak1)
- ik neem dan de [leiding](taak1)
- een soort [vliegende keep](taak1)
- wat ik deed? Dat was alles [regelen](taak1)
- ik hield het in de [gaten](taak1)
- ik had het [overzicht](actie1) en [stuurde](taak1) mensen aan
- ik kan niet goed tegen [stress](situatie1), dus ik houd de [controle](actie1)
- veel facturen [versturen](taak1)  en dat is erg [leuk](situatie1) om te doen
- veel [e-mail](taak1) versturen bijvoorbeeld
+ Vind je dat een essentieel onderdeel van je functie? >her.actie1
+ Kun je iets meer zeggen over de manier waarop je die taak uitvoert? >her.actie1
+ Maar welke rol had je dan precies, heb je meer details over wat je deed? >her.actie1

## intent:taken-duiker
- ik doe gewoon [normaal](taak1) mijn werk
- ik [mijd](taak1) de drukte
- wat ik deed? [vrij nemen](taak1)
- ik [bescherm](taak1) mijn werk, zodat er niets mis gaat
- wat ik deed? Een beetje [rust](taak1) nemen natuurlijk
- eerst maar eens de kat uit de boom [kijken](taak1)
- als er veel [post](taak1) uit moet, dat is wel [druk](situatie1) ja
+ Waarom vind je dat belangrijk: (her.taak1)? >her.actie1
+ Kun je iets meer zeggen over de manier waarop je dat dan deed? >her.actie1
+ Maar welke rol had je dan precies, heb je meer details? >her.actie1

## intent:taken-initiatiefnemer
- dan ga ik spullen [opruimen](taak1) of zo
- achterstand [wegwerken](taak1)
- [wegwerken](taak1) van achterstand 
- op [zoek](taak1) naar meer werk
- [opruimen](taak1) van het archief
- drukte vind ik niet erg, ik kan dan altijd wel iets [nieuws](taak1) doen
- dan leef ik op en [pak](taak1) alles aan
- een voorbeeld is dat er [post](taak1) uit moet
+ Vind je dat een essentieel onderdeel van je rol? >her.actie1
+ Maar leidden je acties niet ook tot onbedoelde effecten? >her.actie1

## intent:taken-sociaal
- tijd nemen voor [praten](taak1) met collega's
- een beetje [rondhangen](taak1) op andere afdelingen
- collega's helpen als er veel [post](taak1) uit moet
+ Vind je dat een belangrijk onderdeel van je rol? >her.actie1

## intent:acties-leider
- Zeker hoort dat erbij [sterker](actie1) nog dat moet
- Dat [vind ik](actie1) wel ja
+ Hoe zou je een hectische omgeving omschrijven? Wat is dat voor jou?>her.situatie1

## intent:acties-duiker
- ik deed dat door [niet op te vallen](actie1)
+ kun je iets meer zeggen over de situatie uit het voorbeeld?>her.situatie1

## intent:acties-initiatiefnemer
- [initiatief](actie1) nemen is wat ik dan doe
- wat bij mij het beste past is [negeren](actie1) en juist doen wat noodzakelijk is
+ Kan dat in elke situatie? Omschrijf de situatie eens waarin dat passend is?>her.situatie1
+ Kan dat in elke situatie? Omschrijf de situatie eens waarin dat zeker niet past?>her.situatie1

## intent:acties-sociaal
- soms [regel](actie1) ik dan iets van [pizza's](resultaat1) of zo
- [samen](actie1) de uitdaging aangaan is wat ik belangrijk vind
- [samen](actie1) bereik je [meer](resultaat1)
+ Dat lukt vast niet in elke situatie, kun je er een omschrijven waarin je wat anders deed?>her.situatie1
+ Is dat altij de beste manier, (her.actie1)? Heb je voorbeelden van een situatie waarin je dat anders aanpakte?>her.situatie1

## intent:resultaat-leider
- ik had een grote [klant](resultaat2) binnengehaald
- Collega's verkozen mij tot [collega](resultaat2) van de maand
- ik heb een klant goed [geholpen](resultaat2)
- het [artikel](resultaat2) is eindelijk af
+ Wat heb je daarvoor precies gedaan? Heb je wat details?>her.actie2
+ Dat is mooi, maar wat deed je exact daarvoor?>her.actie2

## intent:resultaat-duiker
- ik kom al een maand lang op [tijd](resultaat2)
- mijn [lunch](resultaat2) sla ik niet meer over
- ik ga niet meer zo lang [wandelen](resultaat2) tussen de middag
+ Dat is inderdaad fraai, kun je vertellen wat je daarvoor allemaal moest doen?>her.actie2
+ Wat een prestatie zeg, (her.resultaat2), kun je precies vertellen wat je daarvoor deed?>her.actie2

## intent:resultaat-initiatiefnemer

## intent:resultaat-sociaal

## intent:notthereyet
- wab abw als enme wka kd
+ Kun wat meer vertellen over de situatie? >her.situatie1
+ Ik wil wat meer detail horen over de situatie. >her.situatie2
+ Dat is wel duidelijk, maar kun je meer zeggen over het resultaat? >her.resultaat1
+ Ok, dat hoor ik, maar ik wil meer weten over het resultaat dat je bereikte. >her.resultaat2

## intent:bedoeljedat
- bedoel je [met mij in die rol](betekenis)
- bedoel je [dat ik dat doe](betekenis)
- wil je [mijn rol](betekenis)
+ *ja, inderdaad.

## intent:onbegrepen
- wat [bedoel](begrip) je
- dat [snap](begrip) ik niet
- wat [bedoel](begrip) je precies
- ik [begrijp](begrip) je niet
- dat [volg](begrip) ik niet
+ nou ja, direct naar wat ik wil weten; heb je wel altijd het resultaat voor ogen bij wat je doet?>her.resultaat1
+ ik wil uiteindelijk graag weten of je met de situaties hier kunt omgaan, kun je een voorbeeld geven van een situatie waarin je goed functioneert?>her.situatie2
+ hm, hm, ik wil weten hoe je omgaat met moeilijke situaties, kun je er eentje schetsen die je functioneren sterk beperkte?>her.situatie3
+ het is allemaal leuk en aardig, maar of je opgewassen bent tegen de taken hier, weet ik pas als je vertelt wat jouw meest favoriete taak zou zijn.>her.taak4

## intent:herhalen
- watte
- [nog eens](herhaling)
- kun je dat [herhalen](herhaling)
- kun je dat [nog eens zeggen](herhaling)
- wat zei je
- wat zeg je
- ik heb het niet goed [verstaan](herhaling)
+ OK, (me.lastoutput)

## intent:leeftijdvraag
- hoe oud ben jij eigenlijk
- wat is jouw leeftijd
+ ik ben (me.leeftijd). hoe oud ben jij? >her.leeftijd
+ ik ben (me.leeftijd). wat is jouw leeftijd? >her.leeftijd
+ mijn leeftijd is (me.leeftijd). en die van jou? >her.leeftijd

; hieronder herstarten we de 4 gesprekken, voor zover dat niet al gebeurd is, vooral op te lossen clues worden geselecteerd
## intent:leeftijd
- ik ben [32](leeftijd)
- [21](leeftijd) jaar ben ik
- mijn leeftijd is [19](leeftijd)
- [45](leeftijd) is mijn leeftijd
- [dat](leeftijd) zeg ik liever niet
- ik weet niet waarom [leeftijd](leeftijd) relevant zou zijn
- waarom wil je mijn [leeftijd](leeftijd) weten
- [jong](leeftijd) genoeg zou ik zeggen
+ duidelijk. Kunnen we het over iets anders hebben? Je verdere ervaring met de functie (me.vacature)?>her.taak1
+ OK. Kun je iets zeggen over je resultaten in het verleden in de rol van (me.vacature)?>her.resultaat2
+ Juist. Kun je iets zeggen over je ervaring met de functie (me.vacature), als in wat je taak was?>her.taak3
+ begrepen. Over de functie (me.vacature) hier, maar vooral in jouw verleden, vertel eens meer over wat je deed?>her.actie4

## intent:de-weg-kwijt
- snap je het niet
- uhm
+ *sorry, ik begrijp je niet, kun je dat op een andere manier omschrijven?
+ *hm, en verder

## clue:naam
p 0
v harald
## clue:geslacht
p 10
v m
## clue:leeftijd
p 20
v 31
## clue:functie
p 100
v medewerker pz
## clue:werkzaamheden
p 10
v selecteren van nieuw personeel
## clue:vacature
p 100
v medewerker finance
## clue:verkeersissues
p 8
v ja
## clue:wilgesprek
p 10
v ja
## clue:werkgever
p 100
v Jansen en ko
## clue:lastoutput
p 100
v uhm
## clue:lastinput
p 100
v uhm
## clue:situatie1
p 15
v placeholder
## clue:situatie2
p 20
v placeholder
## clue:taak1
p 15
v placeholder
## clue:taak2
p 20
v placeholder
## clue:resultaat1
p 15
v placeholder
## clue:resultaat2
p 20
v placeholder
## clue:actie1
p 15
v placeholder
## clue:actie2
p 20
v placeholder

## clue:naam
p 0
v stephanie
## clue:geslacht
p 10
v v
## clue:leeftijd
p 20
v 39
## clue:functie
p 100
v corporate recruiter
## clue:werkzaamheden
p 10
v werven van nieuw personeel
## clue:vacature
p 100
v medewerker finance
## clue:verkeersissues
p 8
v ja
## clue:wilgesprek
p 10
v ja
## clue:werkgever
p 100
v van Dijk Profielen
## clue:lastoutput
p 100
v uhm
## clue:lastinput
p 100
v uhm
## clue:situatie1
p 15
v placeholder
## clue:situatie2
p 20
v placeholder
## clue:taak1
p 15
v placeholder
## clue:taak2
p 20
v placeholder
## clue:resultaat1
p 15
v placeholder
## clue:resultaat2
p 20
v placeholder
## clue:actie1
p 15
v placeholder
## clue:actie2
p 20
v placeholder
