package main


import (
        "context"
        "fmt"
        "io"
        "log"
        "os"
        "errors"
        "strings"
        "time"
        "flag"
        "regexp"

        speech "cloud.google.com/go/speech/apiv1"
        speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)
func Wordcount(value string) int {
   re := regexp.MustCompile( `[\S]+` )
   results := re.FindAllString( value, -1 )
   return len( results )
}

func main() {
  var filename,         encodingname,     sampleratename  string
  var maxutterancetime, maxutterancecnt, silencewatermark int
  var verbose bool
  flag.StringVar( &filename    , "f", "",
                  "Name of audio file")
  flag.StringVar( &encodingname    , "e", "LINEAR16",
                  "Name of audio encoding, only LINEAR16 and OGG_OPUS are supported")
  flag.StringVar( &sampleratename  , "r", "16000",
                  "Samplerate of audio, only 8k, 16k, 32k and 48k are supported")
  flag.IntVar(    &maxutterancetime, "T", 60,
                  "(NOT YET IMPLEMENTED) Maximum time to process a single utterance in seconds")
  flag.IntVar(    &maxutterancecnt , "C", 160,
                  "Maximum number of words to process in a single utterance")
  flag.IntVar(    &silencewatermark, "S", 900,
                  "Number of milliseconds of no words considered as silence")
  flag.BoolVar(   &verbose, "v", false,
                  "Add more messaging to stderr")
  flag.Parse()

  audioencoding := speechpb.RecognitionConfig_LINEAR16
  switch encodingname { // see https://cloud.google.com/speech-to-text/docs/encoding
   case "LINEAR16":
        audioencoding = speechpb.RecognitionConfig_LINEAR16
   case "OGG_OPUS":
        audioencoding = speechpb.RecognitionConfig_OGG_OPUS
   default:
      log.Fatal( errors.New( "Unsupported audioencoding selected" ) )
  }
  var samplerate int32
  samplerate = 16000
  switch sampleratename { // see https://cloud.google.com/speech-to-text/docs/encoding
   case "8k", "8000":
        samplerate = 8000
   case "16k", "16000":
        samplerate = 16000
   case "32k", "32000":
        samplerate = 32000
   case "48k", "48000":
        samplerate = 48000
   default:
      log.Fatal( errors.New( "Unsupported samplerate selected" ) )
  }

  if verbose {
    log.Printf( "Capturing audio with encoding %s, sampleing at %d Hz\n", encodingname, samplerate )
  }
  ctx := context.Background()

  client, err := speech.NewClient(ctx)
  if err != nil {
     log.Fatal(err)
  }
  stream, err := client.StreamingRecognize(ctx)
  if err != nil {
     log.Fatal(err)
  }
  if err := stream.Send(&speechpb.StreamingRecognizeRequest{
     StreamingRequest: &speechpb.StreamingRecognizeRequest_StreamingConfig{
        StreamingConfig: &speechpb.StreamingRecognitionConfig{
          Config: &speechpb.RecognitionConfig{
                  Encoding:        audioencoding,
                  SampleRateHertz: samplerate,
                  LanguageCode:    "nl-NL",
                  MaxAlternatives: 1,
          },
          SingleUtterance: false,
          InterimResults: true,
        },
     },
  }); err != nil {
          log.Fatal(err)
  }

  InputFile := os.Stdin
  if filename != "" {
    if InputFile, err = os.Open( filename ); err != nil {
      log.Fatal( err )
    }
  }
  go func() {
    buf := make([]byte, 1024)
    for {
       n, err := InputFile.Read(buf)
       if n > 0 {
          if err := stream.Send(&speechpb.StreamingRecognizeRequest{
             StreamingRequest: &speechpb.StreamingRecognizeRequest_AudioContent{
                     AudioContent: buf[:n],
             },
          }); err != nil {
             log.Printf("Could not send audio: %v", err)
          }
       }
       if err == io.EOF {
          if err := stream.CloseSend(); err != nil {
             log.Fatalf("Could not close stream: %v", err)
          }
          return
       }
       if err != nil {
          log.Printf("Could not read from stdin: %v", err)
          continue
       }
    }
  }()

  completeNotFinalUtterance := ""
  printout := time.NewTimer( 59 * time.Second )
  for {
    resp, err := stream.Recv()
    if err == io.EOF {
      break
    }
    if err != nil {
      log.Fatalf("Cannot stream results: %v", err)
    }
    if err := resp.Error; err != nil {
      if err.Code == 3 || err.Code == 11 {
              log.Print("WARNING: Speech recognition request exceeded limit of 60 seconds.")
      }
      log.Print("Could not recognize: %v", err)
      os.Exit( 0 )
    } // hm, this is solved in a much better way in TEAM

    completeNotFinalUtterance = ""

    for _, result := range resp.Results {
      for _, alt := range result.Alternatives {
        if !result.IsFinal {
           completeNotFinalUtterance = completeNotFinalUtterance + " " +
                                       strings.TrimLeft( alt.GetTranscript(), " " )
           if verbose {
             log.Printf( "...: %s", completeNotFinalUtterance )
           }
           //log.Printf( "Stop timer\n" )
           if Wordcount( strings.TrimLeft( completeNotFinalUtterance, " " ) ) >= maxutterancecnt {
             toutter := strings.TrimLeft( completeNotFinalUtterance, " " )
             if verbose {
                log.Printf( "Maxutterancecnt exceeded with result %s\n", toutter )
             }
             _, err := fmt.Printf("%s,\n", toutter  )
             if err != nil { // probably EOF (end of input) or EPIPE (end of output), don't worry, but fail anyway
               os.Exit( 1 )
             }
             os.Exit( 0 ) // chickening out, as not exiting makes partial utterances come out double and double, GCP
           }
           if !printout.Stop() {
              <- printout.C
           }
           printout = time.AfterFunc( time.Duration( silencewatermark ) * time.Millisecond,
              func() {
                 toutter := strings.TrimLeft( completeNotFinalUtterance, " " )
                 if verbose {
                   log.Printf( "Timerevent occured with result %s\n", toutter )
                 }
                 _, err := fmt.Printf("%s,\n", toutter  )
                 if err != nil { // probably EOF (end of input) or EPIPE (end of output), don't worry, but fail anyway
                   os.Exit( 1 )
                 }
                 os.Exit( 0 ) // chickening out, as not exiting makes partial utterances come out double and double, GCP
              })
        } else {
          toutter := strings.TrimLeft( alt.GetTranscript(), " ")
          if verbose {
            log.Printf( "Result [%s], final.\n", toutter )
          }
          if toutter != "" {
            _, err := fmt.Printf("%s\n", toutter )
            if err != nil { // probably EOF (end of input) or EPIPE (end of output), don't worry, but fail anyway
              os.Exit( 1 )
            } else {
              os.Exit( 0 ) // exit for: we did something!
            }
          }
        }
        break // we only need the top alternative
      }
    }
  }
}
