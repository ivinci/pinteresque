package main


// for checking credits use: wget -q -O - "https://tts.readspeaker.com/a/speak?key=$RSAPIKEY&command=credits" | grep amount
// for checking speakers use: wget -q -O - "https://tts.readspeaker.com/a/speak?key=$RSAPIKEY&command=speakers"
// for checking voices use: wget -q -O - "https://tts.readspeaer.com/a/speak?key=$RSAPIKEY&command=voiceinfo"
// forc checking statistics use: wget -q -O - "https://tts.readspeaer.com/a/speak?key=$RSAPIKEY&command=statistics"
import (
 "log"
 "io"
 "os"
 "errors"
 "strings"
 "gopkg.in/resty.v1"
)

func main() {
 voice := "Ilse" // the Dutch female voice
 text := ""
 switch len( os.Args ) {
  case 3:  // gender and text given as parameter
    switch strings.ToLower( os.Args[ 1 ] ) {
     case "m", "♂", "":
       voice = "Guus"
       text = os.Args[ 2 ]
     case "v", "♀", "f":
       voice = "Ilse"
       text = os.Args[ 2 ]
     default:
       text = text + os.Args[ 1 ] + " " + os.Args[ 2 ]
    }
 }

 key := os.Getenv( "RSAPIKEY" )
 if len( key ) < 10 {
   log.Printf( "Bad RSAPIKEY set in Environment\n" )
   panic( errors.New( "NO RSAPIKEY set" ) )
 }

 resp, err := resty.
   SetRedirectPolicy(resty.FlexibleRedirectPolicy(5)).
   R().
   SetQueryParams(map[string]string{
     "key"         : key,
     "lang"        : strings.ToLower( "nl_NL" ),
     "voice"       : voice,
     "text"        : text,
     "audioformat" : "pcm",
     "samplerate"  : "16000",
     "streaming"   : "0",
   }).
   SetDoNotParseResponse( true ).
   Get( "http://tts.readspeaker.com/a/speak" )

 if err != nil {
    log.Printf( "Error received from Readspeaker API: [%v]", err)
 } else {
    reader := resp.RawBody()
    buf := make( []byte, 256 )
    for {
      var nread int
      if n, err := reader.Read( buf ); err != nil && err != io.EOF {
        panic( err )
      } else {
        nread = n
      }
      if nread > 0 {
        if _, err := os.Stdout.Write( buf[ :nread ] ); err != nil {
          panic( err )
        }
      } else {
        reader.Close()
        break
      }
    }
 }
}
