package trail


import (
    "github.com/go-pg/pg"
)

type Trail struct {
  Intentname    string
  Persona       int
  Person        int
  Clue          int
  Pintext       int
  Intent        string // is a json object
  Output        string
}

func AddToTrail( db *pg.DB,
                 persona int, person int, pintext int,
                 intentname string,
                 intent string,
                 output string ) error {
  var row Trail
  row.Intentname = intentname
  row.Persona    = persona
  row.Person     = person
  row.Pintext    = pintext
  row.Intent     = intent
  row.Output     = output
  _, err := db.QueryOne( &row, `
                 insert into trail
                          (persona,person,pintext,intentname,intent,output)
                   values (?persona,?person,?pintext,?intentname,?intent,?output)
                   `, row )
  return err
}
