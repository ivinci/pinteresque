package main


  import (
    "fmt"
    "log"
    "os"
    "os/exec"
    "time"
    "io"
    "strings"
    "strconv"
    "errors"
    "encoding/json"
    "github.com/go-pg/pg"
    "gitlab.com/jhelberg/rasa"
    "pinteresque/persons"
    "pinteresque/clues"
    "pinteresque/texts"
    "pinteresque/trail"
    "gitlab.com/jhelberg/frog"
     flg "github.com/jessevdk/go-flags"
  )

  // while receiving nothing from NLU, we break the silence after some time
  const AcceptableSilence = 30 * time.Second
  var SilenceBreaker = rasa.IntentContainer{
                        Intent: rasa.Intent{
                           Name: "none",
                           Confidence: 1.0,
                        },
                        Text: "silencebreaker",
                     }
  var accepting = false
  var options struct {
    NoSilencebreaker bool `short:"b" description:"Silence will not be interrupted by a new question"`
    Model            string `short:"m" description:"Model to start with" default:"kappersklant" env:"MODEL"`
    Startself        bool `short:"s" description:"Start the model without waiting for a command"`
    Verbose          bool `short:"v" description:"Add some messaging to stderr" env:"VERBOSE"`
  }

  func main() {
    _, err := flg.Parse( &options )
    if err != nil {
      log.Printf( "(pinteresque) Error parsing options: %v", err )
      os.Exit( 1 )
    }
    model := options.Model
    log.Printf( "Starting model %s\n", model )
    doSbrkr := !options.NoSilencebreaker
    startself := options.Startself
    accepting = options.Startself
    if startself {
      log.Printf( " accepting, starting self\n" )
    } else {
      log.Printf( "Not accepting,Not starting self\n" )
    }
    db := pg.Connect( &pg.Options{ User: "pin", Password: "ok now", Database: "pin", Addr: "localhost:5432", } )
    defer db.Close()

    frog := newFrog()
    if frog != nil {
      defer frog.Connection.Close()
    }

    dontunderstandUtterance := getStandardUtterances( db, model )
    persona, person, gender := makePersonAndPersona( db, model )
    prevgender := gender
    defer persons.EndAPerson( db, persona )
    defer persons.EndAPerson( db, person )
    if model == "sorry" {
      if prevgender == "" {
        prevgender = "v"
        log.Printf( "Weird, prevgender is empty, defaulting to v" )
      }
    }
    var clueFindingText texts.Texts // when input comes in, we need to know
    var textUsed        texts.Texts //   on what text, we keep it in these two
    if startself {
      textUsed = doFirstOutput( db, model, persona, person, gender )
      clueFindingText = textUsed
    } else {
      log.Printf( "Not starting self\n" )
    }

    var silenceBreaker *time.Timer  // this is where the timer sits
    if doSbrkr {
      silenceBreaker = time.NewTimer( AcceptableSilence )
      go func() {
         for {
           _, ok := <- silenceBreaker.C
           if !ok {
              return
           }
           if !accepting {
              return
           }
           _, firstOutput := findSuitableText( db, model, SilenceBreaker,
                          persona, person, "noclue", "", "?",
                          dontunderstandUtterance )
           log.Printf( "Issueing output from silenceBreaker: %s\n", firstOutput )
           // text must be a question
           fmt.Printf( gender + " " + firstOutput )
           fmt.Printf( "\n" )
         }
      }()
    }
    for true { // in this loop, calls cannot fail, keep listening and talking
       gender = prevgender
       log.Printf( "reading stdin\n" )
       theline, err := readLine()
       log.Printf( "done reading stdin [%s]\n", theline )
       if err != nil && err == io.EOF { // well, except if input stops, we bail out
          persons.EndAPerson( db, persona )
          persons.EndAPerson( db, person )
          break
       }
       if doSbrkr {
         if !silenceBreaker.Stop() {
            numevents := len( silenceBreaker.C )
            if numevents > 0 {
               if numevents == 1 {
                 <- silenceBreaker.C
               } else {
                 log.Printf( "Strange: silenceBreaker has several (%d) events queued", numevents )
               }
            }
         }
         _ = silenceBreaker.Reset( AcceptableSilence )
       }
       var theIntent rasa.IntentContainer

       if err := unMarshallJson( []byte( theline ), &theIntent ); err != nil {
          // we may receive pinteresque commands from NLU as non-json text.
          words := strings.Split( strings.TrimRight( theline, "\n" ), " " )
          if len( words ) < 1 {
             fmt.Fprintf( os.Stderr, "error understanding json (receiving %v) on Stdin: %v", words, err )
             continue
          }
          switch words[ 0 ] {
            case "model":
                log.Printf( "Restarting for model [%s]\n", words[ 1 ] )
                model = words[ 1 ]
                persons.EndAPerson( db, persona )
                persons.EndAPerson( db, person )
                dontunderstandUtterance = getStandardUtterances( db, model )
                persona, person, gender = makePersonAndPersona( db, model )
                defer persons.EndAPerson( db, persona )
                defer persons.EndAPerson( db, person )
                if model == "sorry" {
                  texts.Output( gender, "tput clear" )
                }
                textUsed = doFirstOutput( db, model, persona, person, gender )
                clueFindingText = textUsed
                if doSbrkr {
                  if !silenceBreaker.Stop() {
                     numevents := len( silenceBreaker.C )
                     if numevents > 0 {
                        if numevents == 1 {
                          <- silenceBreaker.C
                        } else {
                          log.Printf( "Strange: silenceBreaker has several (%d) events queued", numevents )
                        }
                     }
                  }
                  _ = silenceBreaker.Reset( AcceptableSilence )
                }
                accepting = true
            case "end":
                log.Printf( "Ending conversation with id: %d\n", person.Id )
                textUsed = doByeByeOutput( db, model, persona, person, gender )
                clueFindingText = textUsed
                persons.EndAPerson( db, persona )
                persons.EndAPerson( db, person )
                if doSbrkr {
                  _ = silenceBreaker.Stop()
                }
                accepting = false
            default:
              fmt.Fprintln( os.Stderr, "error understanding pin-command on Stdin: [%v]", words )
          }
          continue // it makes no sense to process the intent, it is not there!
       }
       if !accepting {
         log.Printf( "Continuing because of not accepting, skipping %v", theIntent )
         continue
       }
       clues.UpdateIfExists( db, persona.Id, persona.Model, "lastinput", theIntent.Text )  // make (me.lastinput) available

       sents, err := frog.Analyse( theIntent.Text ) // analyse one single sentence
       if err != nil {
         log.Printf( "Analysing text [%s] using frog failed [%v]\n", theIntent.Text, err )
       } else {
         log.Printf( "Frog says [%v]\n", sents[ 0 ] )
         if len( sents ) > 1 {
           log.Printf( "Frog returned more than one line! Weird\n" )
         }
       }
       intentAnalysis := sents[ 0 ] // use result for the first (and hopefully only) sentence

       var cluesSolvedCount int
       intentname := ""
       cluename := ""
       if cluesSolvedCount, cluename, intentname, err = clues.FindSaveCluesFromIntent( db,
                 person.Id, person.Model, theIntent, intentAnalysis ); err != nil {
         log.Printf( "FindSaveCluesFromIntent (db, %v, %v) failed [%v]\n", person, theIntent, err )
       } else {
         log.Printf( "Saved %d clues from intent, clue[%s] intent[%s], cluetoinduce: [%v or %v]\n",
              cluesSolvedCount, cluename, theIntent.Intent.Name, clueFindingText.Cluetoinduce, textUsed.Cluetoinduce )
         theIntent.Intent.Name = intentname
       }
       // if solved is 0; try harder in case there is an induced clue.
       // we may even have induced two clues:
       // one from the textUsed and one from clueFindingText
       // try both (if not similar) and if Cluetoinduce != ""
       if cluesSolvedCount == 0  { // accept answer is for this clue, try harder
         if clueFindingText.Cluetoinduce != "" {
           if count, cname, intentname, err :=
              clues.SaveClueFromIntent( db, person.Id, person.Model, theIntent.Text,
                                        theIntent.Intent.Name,intentAnalysis,
                                        clueFindingText.Cluetoinduce ); err != nil {
             log.Printf( "Knowing clue (%s), erroring out on SaveClueFromIntent with: %v\n",
                          clueFindingText.Cluetoinduce, err )
           } else {
             cluesSolvedCount += count
             theIntent.Intent.Name = intentname
             cluename = cname
           }
         }
         if textUsed.Cluetoinduce != "" && textUsed.Cluetoinduce != clueFindingText.Cluetoinduce {
           if count, cname, intentname, err :=
                clues.SaveClueFromIntent( db, person.Id, person.Model, theIntent.Text,
                                          theIntent.Intent.Name, intentAnalysis,
                                          textUsed.Cluetoinduce ); err != nil {
             log.Printf( "Knowing clue (%s), erroring out on SaveClueFromIntent with: %v\n",
                          textUsed.Cluetoinduce, err )
           } else {
             cluesSolvedCount += count
             if textUsed.Cluetoinduce == "her.naam" ||
                textUsed.Cluetoinduce == "naam" { // special case, should generalize
               theIntent.Intent.Name = "naam"
             } else {
               theIntent.Intent.Name = intentname
               cluename = cname
             }
           }
         }
         if cluesSolvedCount == 0 {
           log.Printf( "Knowing the clue (%s or %s or %s), still could not get it from from intent [%v]\n",
                        clueFindingText.Cluetoinduce, textUsed.Cluetoinduce,
                        cluename, theIntent.Text  )
         }
       }

       intentIsQuestion, err := intentAnalysis.IsQuestion()
       if err != nil && intentIsQuestion && cluesSolvedCount > 0 {
         log.Printf( "Solved clues from a question, weird [%s]\n", theIntent.Text )
       }
          // now find words to speak, possibly using clues just discovered
       firstOutput := ""
       textUsed, firstOutput = findSuitableText( db, model, theIntent, persona, person, cluename,
                                                  theline, "", dontunderstandUtterance )
       if model == "sorry" && textUsed.Modifier == "%" {
         // we're done talking, maybe generate a report and go back to wait for next call
         log.Printf( "In model sorry, finish-text uttered, not accepting any incoming speech (text) until introduction" )
         log.Printf( "Some report with (person,persona) tuple: (%d,%d)",  person.Id, persona.Id )
         p := strconv.Itoa( person.Id )
         pa := strconv.Itoa( persona.Id )
         //persons.EndAPerson( db, persona )
         //persons.EndAPerson( db, person )
         cmd := exec.Command( "bin/makeanexcuse", p, pa )
         if err := cmd.Run(); err != nil { // maybe use Start and pick Wait in a go-routine
           log.Printf( "(sorry) makeanexcuse failed [%v]\n", err )
         }
         accepting = false
         texts.Output( prevgender, firstOutput )
         log.Printf( "Session ended; continuing because of not accepting" )
         continue
       }
       log.Printf( "Text to utter has modifier: %s", textUsed.Modifier )
       texts.Output( prevgender, firstOutput )
       if !accepting {
          log.Printf( "should not occur: Continuing because of not accepting" )
          continue
       }

       secondOutput := ""
       if !textUsed.IsQuestion() && cluesSolvedCount < 1 {
         // i.e. we havent solved clues yet, press for more clue-solving!
         // but dont bother if we asked a question in the first part of our text.
         // it is not nice to ask two questions in one turn.
         clueFindingText, secondOutput = findSuitableClueInducer( db, model, theIntent,
                                persona, person, firstOutput, theline)
      }
      if model == "sorry" && textUsed.Modifier == "%" {
        log.Printf( "In model sorry and received clue-inducing finish-text, weird" )
      }
      texts.Output( prevgender, secondOutput )

      clues.UpdateIfExists( db, persona.Id, persona.Model, "lastoutput",
                            firstOutput + " " + secondOutput )

      log.Println( "" )
    }
  }

  func doFirstOutput( db *pg.DB, model string, persona persons.Persons,
                      person persons.Persons, personagender string ) ( texts.Texts ) {
    if first, err := texts.StartText( db, model ); err == nil {
      output := first.ReduceText( db, persona.Id, person.Id, person.Model )
      log.Printf( "Uttering first [%s]", output )
      texts.Output( personagender, output )
      clues.UpdateIfExists( db, persona.Id, persona.Model, "lastoutput", output )
      trail.AddToTrail( db, persona.Id, person.Id, first.Id,
                        "", "", output )
      return first
    } else {
      return texts.Texts{}
    }
  }
  func doByeByeOutput( db *pg.DB, model string, persona persons.Persons,
                      person persons.Persons, personagender string ) ( texts.Texts ) {
    if first, err := texts.GoingAwayText( db, model ); err == nil {
      output := first.ReduceText( db, persona.Id, person.Id, person.Model )
      log.Printf( "Uttering byebye [%s]", output )
      texts.Output( personagender, output )
      clues.UpdateIfExists( db, persona.Id, persona.Model, "lastoutput", output )
      trail.AddToTrail( db, persona.Id, person.Id, first.Id,
                        "", "", output )
      return first
    } else {
      return texts.Texts{}
    }
  }


  func unMarshallJson( theline []byte, theIntent *rasa.IntentContainer ) (err error) {
       if err = json.Unmarshal( []byte( theline ), &theIntent ); err != nil {
          return err
       }
       theIntent.Text = strings.TrimRight( theIntent.Text, "\n" )
       if theIntent.Text == "" {
          log.Println( "Error unmarhsallJson; empty input" )
          return errors.New( "JSON-format error: Empty input encountered" )
       }
       log.Printf( "Going in with intent [%v] with slots [%v] for text [%s]\n",
                   theIntent.Intent, theIntent.Entities, theIntent.Text )
       return nil
  }

  func findSuitableClueInducer( db *pg.DB, model string, theIntent rasa.IntentContainer,
                                persona persons.Persons, person persons.Persons,
                                firstOutput string,
                                theline string ) ( texts.Texts, string ) {

     var clueFindingText texts.Texts
     secondOutput := ""
     clue, err := clues.TopClue( db, person.Id, model )
     if err != nil {
         log.Printf( "Error getting TopClue: %v?\n", err )
         return clueFindingText, ""
     } else if err == nil && len( clue.Name ) > 0 {
        if clueFindingText, err = texts.TextForSolvingClue( db, clue.Name, model ); err == nil {
           if textToAdd := clueFindingText.ReduceText( db, persona.Id, person.Id, person.Model );
              textToAdd != firstOutput {
              log.Printf( "Adding TextForClue: %v?\n", clueFindingText )
              secondOutput += " " + textToAdd
              trail.AddToTrail( db, persona.Id, person.Id, clueFindingText.Id,
                       theIntent.Intent.Name, theline,
                       secondOutput )
           }
        }
     }
     return clueFindingText, secondOutput
  }

  func findSuitableText( db *pg.DB, model string, theIntent rasa.IntentContainer,
               persona persons.Persons, person persons.Persons, clue string,
               theline string, demand string,
               dontunderstandUtterance texts.Texts ) ( texts.Texts, string ) {
       var textUsed texts.Texts
       firstOutput := ""
       if mytexts, err := texts.TopTexts( db, model, theIntent.Intent.Name,
                                          persona.Id, person.Id, clue );
          err != nil {
          log.Printf( "TopTexts failed [%v]", err )
          firstOutput = ""
       } else {
          log.Printf( "Toptexts returned %d texts\n", len( mytexts ) )
          for _, thetext := range mytexts {
             log.Printf( "Toptexts returned [%v]\n", thetext )
          }
          textUsed = dontunderstandUtterance
          firstOutput = textUsed.Content
          for _, thetext := range mytexts {
             //log.Printf( "Considering a text from TopTexts: [%v], demanding [%s]\n",
             //            thetext, demand )
             if demand == "?" {
                 if !thetext.IsQuestion() {
                     continue
                 }
             } else if demand == "!" {
                 if thetext.IsQuestion() {
                     continue
                 }
             }
             firstOutput = thetext.ReduceText( db, persona.Id, person.Id, person.Model )
             textUsed = thetext
             if !texts.HasClueRefs( firstOutput ) {
                log.Printf( "Using from TopTexts: [%v] as [%s]\n", thetext, firstOutput )
                break
             }
          }
          if texts.HasClueRefs( firstOutput ) { // maybe give this particular clue higher priority
             log.Printf( "No Text left without cleurefs. Skipping [%v]\n", textUsed )
          }
       }
       trail.AddToTrail( db, persona.Id, person.Id, textUsed.Id,
                         theIntent.Intent.Name, theline,
                         firstOutput )
       return textUsed, firstOutput
  }

  func personsGender( db *pg.DB, persona persons.Persons ) string {
    genderClue, err := clues.Clue( db, persona.Id, persona.Model, "geslacht" )
    if err != nil {
      panic( err )
    }
    return genderClue.Value
  }

  func newFrog() *frog.FrogClient {
    connectstring := os.Getenv( "FROG_CONNECT" )
    if connectstring == "" {
         connectstring = "178.79.165.162:8080"
    }
    log.Printf( "Connecting to Frog service at [%s]\n", connectstring )
    if frog, err := frog.NewFrogClient( connectstring ); err != nil {
       log.Printf( "Error %v connecting to Frog service\n", err )
       return nil
    } else {
       return frog
    }
  }

  func readLine() (string, error) {
    var result []byte
    terminator := byte( 0xa )
    buf := make( []byte, 1 )
    for buf[ 0 ] != terminator {
       if _, err := os.Stdin.Read( buf ); err != nil {
          return "", err
       }
       result = append( result, buf[ 0 ] )
    }
    return string( result ), nil
  }

  func getStandardUtterances( db *pg.DB, model string ) texts.Texts {
    if dontunderstandUtterance, err := texts.GetText( db, "sorry, ik begrijp je niet", model );
       err != nil {
      panic( err )
    } else
    {
      return dontunderstandUtterance
    }
  }

func makePersonAndPersona( db *pg.DB, model string ) (persons.Persons, persons.Persons, string ) {
  persona := persons.GetAnyPersona( db, model )
  person := persons.MakeANewPerson( db, model )
  persons.InitCluesForPerson( db, person, model )
  log.Printf( "Persona %d and person %d talking with model %s", persona.Id, person.Id, model )
  return persona, person, personsGender( db, persona )
}
