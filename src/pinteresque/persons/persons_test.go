package persons

import (
    "testing"
    "os"
    "github.com/go-pg/pg"
)

var db *pg.DB

func TestMain(m *testing.M) {

   db = pg.Connect( &pg.Options{
            User: "joost", Password: "ok now", Database: "pin", Addr: "localhost:5432",
       })
   defer db.Close()
   os.Exit(m.Run())
}
func TestPerson( t *testing.T ) {
   var person Persons
   var err error

   if person, err = MakeANewPerson( db, "kp" ); err != nil {
     t.Error( "MakeANewPerson failed", err )
   }
   if person.Id < 3 {
      t.Error( "Failed: person.Id should be > 2" )
   }
   if err = InitCluesForPerson( db, person ); err != nil {
      t.Error( "InitCluesForPerson failed", err )
   }
   if _, err = RunningPerson( db ); err != nil {
      t.Error( "RunningPerson failed", err )
   }
   if err = EndAPerson( db, person ); err != nil {
      t.Error( "EndAPerson for person failed", err )
   }
   if _, err = RunningPerson( db ); err == nil {
      t.Error( "RunningPerson failed, should not return anything, it did though" )
   }
}
func TestPersona( t *testing.T ) {
   var persona Persons
   var err error
   if persona, err = GetAnyPersona( db, "kappersklant" ); err != nil {
      t.Error( "GetAnyPersona failed", err )
   }
   if persona.Id != 1 && persona.Id != 2 {
      t.Error( "Failed: persona.Id should be 1 or 2" )
   }
   if !persona.Isrobot {
      t.Error( "Failed: persona.Isrobot should be true" )
   }
   if persona.Model != "kappersklant" && persona.Model != "kappersklant" {
      t.Errorf( "Failed: persona.Model should be [kappersklant]: [%s]", persona.Model )
   }
   if _, err := StartAPersona( db, persona ); err != nil {
      t.Error( "StartAPersona failed", err )
   }
   if _, err := RunningPersona( db ); err != nil {
      t.Error( "RunningPersona failed", err )
   }
   if err := EndAPerson( db, persona ); err != nil {
      t.Error( "EndAPerson for persona failed", err )
   }
   if _, err := RunningPersona( db ); err == nil {
      t.Error( "RunningPersona failed, should not return anything, it did though" )
   }
}
