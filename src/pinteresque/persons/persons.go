package persons


import (
    "log"
    "github.com/go-pg/pg"
)
type Persons struct {
  Id        int
  Starttime pg.NullTime
  Endtime   pg.NullTime
  Isrobot   bool
  Model     string
}

func GetAnyPersona( db *pg.DB, model string ) ( Persons ) {
  var person Persons
  _, err := db.QueryOne( &person, `
                     select p.id, p.starttime, p.endtime, p.isrobot, p.model
                       from persons p
                      where     p.isrobot
                            and (p.model is null or p.model = ?)
                      order by p.model, random()
                      limit 1
                     `, model )
  if err != nil {
     log.Printf( "GetAnyPersona failed [%v], did you load the right model [%s]?", err, model )
     panic( err )
  }
  return person
}

func StartAPersona( db *pg.DB, person Persons ) ( Persons, error ) {
  _, err := db.QueryOne( &person, `
                     update persons
                        set starttime = now(),
                            endtime = null
                      where id = ?
                     `, person.Id )
  if err == nil {
    _, err  = db.QueryOne( &person, `
                       select p.id, p.starttime, p.endtime, p.isrobot, p.model
                         from persons p
                        where p.id = ?
                       `, person.Id )
  }
  return person, err
}

func EndAPerson( db *pg.DB, person Persons ) error {
  _, err := db.QueryOne( &person, `
                        update persons
                           set endtime = now()
                         where id = ? and endtime is null
                     returning id, starttime, endtime, isrobot, model
                     `, person.Id )
  return err
}

func MakeANewPerson( db *pg.DB, model string ) ( Persons ) {
  var person Persons
  person.Model = model
  _, err := db.QueryOne( &person, `
                     insert into persons ( starttime, endtime, model )
                                  values ( now(), null, ? )
                       returning id
                       `, model )
  if err != nil {
      log.Printf( "MakeANewPerson failed [%v]", err )
      panic( err )
  }
  return person
}

func InitCluesForPerson( db *pg.DB, person Persons, model string ) {
  _, err := db.Query( &person, `
                     insert into clues (person,name,value,priority,model,postags)
                         select ?, name, null, max(priority), ?, max(postags)
                           from clues c
                          where (model is null OR model = ?)
                            and person in (select id
                                             from persons
                                            where isrobot
                                              and coalesce(model,'') = coalesce(c.model,'')
                                            order by id DESC limit 1)
                          group by name
                       `, person.Id, model, model )
  if err != nil {
      panic( err )
  }
  return
}

func RunningPerson( db *pg.DB ) ( Persons, error ) {
  var person Persons
  _, err  := db.QueryOne( &person, `
                      select p.id, p.starttime, p.endtime, p.isrobot, p.model
                        from persons p
                      where     p.endtime is null
                            and not p.isrobot
                      order by p.starttime DESC
                      limit 1
                      `)
  return person, err
}

func RunningPersona( db *pg.DB ) ( Persons, error ) {
  var person Persons
  _, err  := db.QueryOne( &person, `
                      select p.id, p.starttime, p.endtime, p.isrobot, p.model
                        from persons p
                      where     p.endtime is null
                            and p.isrobot
                      order by p.starttime DESC
                      limit 1
                      `)
  return person, err
}


func SetGender( db *pg.DB, person Persons, name string ) ( error ) {
  var gender  string
  type parm struct {
      Person int
      Name   string
  }
  actual := parm{ Person: person.Id, Name: name}
  _, err := db.QueryOne( &gender, `
                    update clues
                       set value = (select sex from names where name ilike ?name order by random() limit 1)
                     where person = ?person and name = 'geslacht' and coalesce(value,'') = ''
                    `, actual )
  if err != nil && err == pg.ErrNoRows {
    return nil
  }
  return err
}
