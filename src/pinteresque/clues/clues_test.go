package clues

import (
    "testing"
    "os"
    "github.com/go-pg/pg"
    "pinteresque/persons"
    "gitlab.com/jhelberg/rasa"
)

var db *pg.DB

func TestMain(m *testing.M) {
   db = pg.Connect( &pg.Options{
            User: "joost", Password: "ok now", Database: "pin", Addr: "localhost:5432",
       })
   defer db.Close()
   os.Exit(m.Run())
}

func TestClues( t *testing.T ) {
   var person persons.Persons
   var err error
   if person, err = getNewPerson( db ); err != nil {
     t.Error( "getting a new Person failed while testing clues", err )
   }
   var clue Clues
   if clue, err = TopClue( db, person.Id, "kappersklant" ); err != nil {
     t.Error( "TopClue failed", err )
   }
   if (clue.Name != "kent knipwens") && (clue.Name != "naam") {
     t.Error( "Clue.Name must be knipwens or naam, failed" )
   }
   if clue.Name == "kent knipwens" {
      clue.Value = "kort amerikaans"
   }
   if clue.Name == "naam" {
      clue.Value = "karel"
   }
   if err = clue.Save( db ); err != nil {
     t.Error( "clue.Save failed", err )
   }
   if clue, err = TopClue( db, person.Id, "kappersklant" ); err != nil {
     t.Error( "TopClue failed", err )
   }
   if (clue.Name != "kent knipwens") && (clue.Name != "naam") && (clue.Name != "geslacht") {
     t.Errorf( "Clue.Name must be knipwens or naam or geslacht, failed: %s", clue.Name )
   }
   if clue.Name == "kent knipwens" {
      clue.Value = "kort amerikaans"
   }
   if clue.Name == "naam" {
      clue.Value = "karel"
   }
   if err = clue.Save( db ); err != nil {
     t.Error( "clue.Save failed", err )
   }
   var count int
   var cluestr string
   //var iname string
   if count, cluestr, _, err = FindSaveCluesFromIntent( db, person.Id, person.Model,
                                                        rasa.IntentContainer{ Intent: rasa.Intent{"leeftijdpos", 0.4},
                                                                              Entities: []rasa.Entity{ {Start:0,End:0,Value:"",Entity:"", Confidence:0.0, Extractor:""} },
                                                                              Text: "vierendertig" }, nil );
      err != nil {
     t.Error( "clue.FindSaveCluesFromIntent for age failed", err )
   }
   if count == 0 {
     t.Error( "getting age from clue.FindSaveCluesFromIntent failed", err )
   }
   if cluestr != "leeftijd" {
     t.Error( "getting 'leeftijd' from clue.FindSaveCluesFromIntent with age", err )
   }

   if err = persons.EndAPerson( db, person ); err != nil {
     t.Error( "EndAPerson for person failed", err )
   }
}

func getNewPerson( db *pg.DB ) ( persons.Persons, error ) {
   var person persons.Persons
   var err error
   person = persons.MakeANewPerson( db, "kp" )
   if person.Id < 3 {
     return person, err
   }
   persons.InitCluesForPerson( db, person, "kappersklant" )
   if person, err = persons.RunningPerson( db ); err != nil {
     return person, err
   }
   return person, err
}
