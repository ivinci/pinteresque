package clues


import (
    "os"
    "fmt"
    "log"
    "strings"
    "strconv"
    "regexp"
    "github.com/go-pg/pg"
    "gitlab.com/jhelberg/rasa"
    "gitlab.com/jhelberg/frog"
)

type Clues struct {
  Id       int
  Person   int
  Name     string
  Value    string
  Priority int
  Pit      pg.NullTime
  Model    string
  Postags  string
}

func Clue( db *pg.DB, personId int, model string, name string ) ( Clues, error ) {
   var clue Clues
   _, err := db.QueryOne( &clue, `
                  select *
                    from clues
                   where person = ? and name = ? and (model is null OR model = ?)
                   limit 1
                  `, personId, name, model )
   if err == pg.ErrNoRows {
      clue.Name = ""
      return clue, nil
   }
   return clue, err
}

func TopClue( db *pg.DB, personId int, model string ) ( Clues, error ) {
   var clue Clues
   _, err := db.QueryOne( &clue, `
                  select *
                    from clues
                   where     person = ?
                         and model = ?
                         and value is null
                   order by priority
                   limit 1
                  `, personId, model )
   if err == pg.ErrNoRows {
      clue.Name = ""
      return clue, nil
   }
   return clue, err
}

func (clue *Clues) Save( db *pg.DB ) ( error ) {
   _, err := db.QueryOne( &clue, `
                  update clues
                     set value = ?, pit = now()
                   where id = ?
                  `, clue.Value, clue.Id )
   return err
}

func UpdateIfExists( db *pg.DB, personId int, model string, name string, value string ) {
  log.Printf( "Saving [%s].\n", name )
  if theClue, err := Clue( db, personId, model, name ); err != nil {
    log.Printf( "No [%s] clue found, not able to update with [%s].\n", name, value )
  } else {
    theClue.Value = value
    theClue.Save( db )
    log.Printf( "[%s] clue saved.\n", name )
  }
}

func findRequiredPostags( db *pg.DB, personId int, name string ) []string {
   reqs := ""
   _, err := db.QueryOne( &reqs, `
                  select coalesce( postags, '' )
                    from clues
                   where person = ? and name = ?
                  `, personId, name )
   if err != nil {
      return []string{}
   }
   return strings.Split( reqs, " " )
}


func SaveAClue( db *pg.DB, value string, name string, personId int, model string ) ( error ) {
   var clue Clues
   type parm struct {
      Value    string
      Name     string
      Person   int
      Model    string
   }
   actual := parm{ Value: value, Name: name, Person: personId, Model: model }
   _, err := db.QueryOne( &clue, `
                  insert into clues
                                (  value,  name,  person, pit, model )
                         values ( ?value, ?name, ?person, now(), ?model )
                  on conflict ON constraint clues_un
                  do
                  update
                     set value = ?value, pit = now()
                   where clues.value is null
                  `, actual )
   return err
}

func FindSaveCluesFromIntent( db *pg.DB, personId int, personModel string,
                              intent rasa.IntentContainer,
                              analysed frog.Sentence,
                            ) (count int, clue string, iname string, err error) {
   count = 0
   nameSlotDone := false
   iname = intent.Intent.Name
   for _, slot := range intent.Entities {
      if slot.Value == "\n" { // slot.Entity is the name (e.g. naam), slot.Value is it's value (e.g. Joost)
        continue
      }
        // the clue may contain several words. We strip the ones not necessary
        // The abstract clue (the persona one) has an empty Postags
        // (anything goes) or contains blank seperated POSTags
        // one of these Postags must match the tok.POSTag to include
        // this as an word in the clue
      slot.Value = stripNonValidWords( db, personId, slot, analysed )
      if err = SaveAClue( db, slot.Value, slot.Entity, personId, personModel ); err != nil {
         if err == pg.ErrNoRows {
             log.Printf( "Did not save clue: (%s,%s) for person %d, is it missing or double?\n",
                          slot.Entity, slot.Value, personId )
             err = nil
         } else {
           return count, slot.Entity, iname, err
         }
      }
      count += 1
      if strings.HasSuffix( intent.Intent.Name, "naam" ) {
        nameSlotDone = true
      }
      log.Printf( "Slots found in intent [%v] for [%d], count is [%d]\n",
                   slot, personId, count )
   }
   if count == 0 {
      log.Printf( "No slots found in intent [%s] for [%d], count is [%d]\n",
                   intent.Intent.Name, personId, count )
   }
   oneWord, err := regexp.MatchString( "^\\w+$", intent.Text )
   if err != nil {
      return count, "", iname, err
   }
   conf := intent.Intent.Confidence
   cluesolved := ""
   switch {
      case !nameSlotDone && conf > 0.3 && strings.HasSuffix( intent.Intent.Name, "naam" ):
         log.Printf( "Trying naam-clue [%s] for [%d], count is [%d]\n",
                      intent.Text, personId, count )
         words := strings.Split( intent.Text, " " )
         verbskipped := false
         for _, word := range words {
            tok := analysed[ word ]
            if !verbskipped && tok.IsVerb() {
               log.Printf( "[%s] is a verb, skipping as name\n", tok.Token )
               verbskipped = true // allow skipping once, after that it may be a name after all
               continue
            }
            if tok.IsGreeting( analysed ) {
               log.Printf( "[%s] is a greeting, skipping as name\n", tok.Token )
               continue
            }
            if ct, err := tryNameClue( db, word, personId, personModel ); err != nil {
                return count, "", iname, err
            } else {
               count += ct
               iname = "naam"
               cluesolved = "naam"
            }
         }
      case oneWord && strings.Contains( intent.Intent.Name, "leeftijd" ):
       log.Printf( "Trying one-word-leeftijd-clue %s for %d, count is %d\n",
                     intent.Text, personId, count )
       tok := analysed[ intent.Text ]
       if len( analysed ) == 0 || tok.IsNumber() {
           log.Printf( "Possible number found: [%s]\n", intent.Text )
           if ct, err := tryAgeClue( db,  intent.Text, personId, personModel ); err != nil {
              return count, "", iname, err
           } else {
             count += ct
             iname = "leeftijd"
             cluesolved = "leeftijd"
           }
       }
      case oneWord: // can be yes, no or sorry
       log.Printf( "Trying one-word-clue [%s] for [%d], count is [%d]\n",
                    intent.Text, personId, count )
       if ct, err := tryNameClue( db, intent.Text, personId, personModel ); err != nil {
          return count, "", iname, err
       } else {
          count += ct
          iname = "naam" // we did solve a name, this must be a naam intent
          cluesolved = "naam"
       }
     default:
       log.Printf( "Giving up on further clue-solving %s for %d, count is %d\n",
                    intent.Text, personId, count )
   }
   return count, cluesolved, iname, nil
}

var reduceCluetoinduce = regexp.MustCompile( "her\\." )

func SaveClueFromIntent( db *pg.DB, personId int, personModel string,
                         intentText string, intentName string, analysed frog.Sentence,
                         clueText string,
                       ) (count int, clue string, iname string, err error) {
  clueName := reduceCluetoinduce.ReplaceAllString( clueText, "" ) // get rid of clue-context (i.e. me./her. etc.)
  intentText = stripNonValidWords( db, personId, rasa.Entity{ Value: intentText, Entity: clueName }, analysed )

  log.Printf( "Willing to save clue [%s] for %d with intentName %s as %s\n",
              clueName, personId, intentName, intentText)
  // we may adapt intent-name because of the clues we find
  if err := SaveAClue( db, intentText, clueName, personId, personModel ); err != nil {
    return 0, "", "", err
  } else {
    return 1, clueName, intentName, nil
  }
}
func FindGender( db *pg.DB, name string ) ( string, error ) {
  var gender  string
  _, err := db.QueryOne( &gender, `
                    select sex
                      from names
                     where name ilike ?
                     order by random()
                     limit 1
                    `, name )
  if err != nil && err == pg.ErrNoRows {
    return "", nil
  }
  return gender, err
}

  // resultingValue := ""
  // for all words in slot.Value
    // pick it's analysed[ word ]
    // and check if this matches the required POSTag in the persona-clue
    // if so, add to resulting Value
func stripNonValidWords( db *pg.DB, personId int, slot rasa.Entity,
                         gramm frog.Sentence ) string {
   val := ""
   log.Printf( "stripping non valid words: [%s,%s]", slot.Entity, slot.Value )
   required := findRequiredPostags( db, personId, slot.Entity )
   if len( required ) == 0 {
     return slot.Value
   }
   log.Printf( "stripping non valid words, required: %v", required )
   for _, word := range strings.Split( slot.Value, " " ) {
      if tok, ok := gramm[ word ]; ok {
        for _, postag := range required {
           if strings.Index( tok.POSTag, postag ) != -1 {
             val = val + " " + word
           }
        }
      } else {
        val = val + " " + word
      }
   }
   log.Printf( "stripped non valid words: [%s,%s]", slot.Entity, strings.TrimSpace( val ) )
   return strings.TrimSpace( val )
}

// here we try to check whether name is a persons name by seeking a gender for this name.
// If this name has no gender, it is not a name.
func tryNameClue( db *pg.DB, name string, personId int, personModel string,
                ) (count int, err error) {
   if gender, err := FindGender( db, name ); err != nil {
     fmt.Fprintf( os.Stderr, "tryNameClue found no gender for [%s] for [%d]\n", name, personId )
     return count, err
   } else if gender != "" {
     fmt.Fprintf( os.Stderr, "tryNameClue naam found [%s] for [%d]\n", name, personId )
     if err = SaveAClue( db, name, "naam", personId, personModel ); err != nil {
       if err == pg.ErrNoRows {
            fmt.Fprintf( os.Stderr, "tryNameClue 1 did not save clue: [%s] [%s] for [%d], is it missing?\n",
                                     "naam", name, personId )
        }
        return count, err
     }
     count += 1
     fmt.Fprintf( os.Stderr, "tryNameClue geslacht found [%s] for [%d]\n", gender, personId )
     if err = SaveAClue( db, gender, "geslacht", personId, personModel ); err != nil {
       if err == pg.ErrNoRows {
            fmt.Fprintf( os.Stderr, "tryNameClue 2 did not save clue: [%s] [%s for [%d], is it missing?\n",
                                     "geslacht", gender, personId )
        }
        return count, err
     }
   }
   return count, nil
}

// we are smarter than this, aren't we?
var tn2dig = map[string]int{
                "een":  1,
                "twee": 2,
                "drie": 3,
                "vier": 4,
                "vijf": 5,
                "zes":  6,
                "zeven": 7 ,
                "zeuven": 7 ,
                "acht":  8,
                "negen":  9,
                "tien":  10,
                "elf":  11,
                "twaalf":  12,
                "twaluf":  12,
                "dertien":  13,
                "veertien":  14,
                "vijftien":  15,
                "zestien":  16,
                "zeventien": 17 ,
                "zeuventien": 17 ,
                "achttien":  18,
                "negentien":  19,
                "twintig":  20,
                "eenentwintig":  21,
                "tweeentwintig":  22,
                "drieentwintig":  23,
                "vierentwintig":  24,
                "vijfentwintig":  25,
                "zesentwintig":  26,
                "zevenentwintig":  27,
                "zeuvenentwintig":  27,
                "achtentwintig":  28,
                "negenentwintig":  29,
                "dertig":  30,
                "eenendertig":  31,
                "tweeendertig":  32,
                "drieendertig":  33,
                "vierendertig":  34,
                "vijfendertig":  35,
                "zesendertig":  36,
                "zevenendertig":  37,
                "zeuvenendertig":  37,
                "achtendertig":  38,
                "negenendertig":  39,
                "veertig":  40,
                "eenenveertig":  41,
                "tweeenveertig":  42,
                "drieenveertig":  43,
                "vierenveertig":  44,
                "vijfenveertig":  45,
                "zesenveertig":  46,
                "zevenenveertig":  47,
                "zeuvenenveertig":  47,
                "achtenveertig":  48,
                "negenenveertig":  49,
                "vijftig":  50,
                "eenenvijftig":  51,
                "tweeenvijftig":  52,
                "drieenvijftig":  53,
                "vierenvijftig":  54,
                "vijfenvijftig":  55,
                "zesenvijftig":  56,
                "zevenenvijftig":  57,
                "zeuvenenvijftig":  57,
                "achtenvijftig":  58,
                "negenenvijftig":  59,
                "zestig":  60,
                "eenenzestig":  61,
                "tweeenzestig":  62,
                "drieenzestig":  63,
                "vierenzestig":  64,
                "vijfenzestig":  65,
                "zesenzestig":  66,
                "zevenenzestig":  67,
                "zeuvenenzestig":  67,
                "achtenzestig":  68,
                "negenenzestig":  69,
                           }
func tryAgeClue( db *pg.DB, word string, personId int, personModel string,
               ) (int, error) {
   var age int
   var err error
   if age, err = strconv.Atoi( word ); err != nil {
     age = tn2dig[ word ]
   }
   fmt.Fprintf( os.Stderr, "tryAgeClue found %d for %s\n", age, word )
   if age > 6 && age < 80 {
     if err = SaveAClue( db, strconv.Itoa( age ), "leeftijd", personId, personModel ); err != nil {
       if err == pg.ErrNoRows {
            fmt.Fprintf( os.Stderr, "tryAgeClue did not save clue: %s %s for %d, is it missing?\n",
                                     "leeftijd", word, personId )
        }
        return 0, err
     }
     return 1, nil
   } else {
     return 0, nil
   }
}
