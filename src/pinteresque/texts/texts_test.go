package texts

import (
    "testing"
    "os"
    "regexp"
    "github.com/go-pg/pg"
    "pinteresque/persons"
    //"clues"
)

var db *pg.DB

func TestMain(m *testing.M) {
   db = pg.Connect( &pg.Options{
            User: "joost", Password: "ok now", Database: "pin", Addr: "localhost:5432",
       })
   defer db.Close()
   os.Exit(m.Run())
}

func TestTexts( t *testing.T ) {
   var person persons.Persons
   var persona persons.Persons
   var err error
   persona = persons.GetAnyPersona( db, "kappersklant" )
   if person, err = getNewPerson( db ); err != nil {
     t.Error( "getting a new Person failed while testing clues", err )
   }

   var ttexts []Texts
   if ttexts, err = TopTexts( db, "kappersklant", "sport", persona.Id, person.Id ); err != nil {
     t.Error(  "TopTexts failed ", err )
     t.Errorf( "         failed for %s, %v %v", "sport", persona, person )
   }
   if len( ttexts ) < 1 {
       t.Errorf( "TopTexts failed, too little proposed texts: %v", ttexts )
   }
   for _, text := range ttexts {
     if len( text.Content ) < 2 {
       t.Errorf( "TopTexts failed, text too short: %s", text.Content )
     }
     //t.Errorf( "Text proposed: %s", text.Content )
   }

   if err = persons.EndAPerson( db, person ); err != nil {
     t.Error( "EndAPerson for person failed", err )
   }

   if HasClueRefs( "äap noot mies" ) {
     t.Error( "HasClueRefs is true, but should not be" )
   }
   if !HasClueRefs( "Waarom vind je dat belangrijk (her.taak1)" ) {
     t.Error( "HasClueRefs is false, but should be true" )
   }
   if !HasClueRefs( "aap (naam) noot" ) {
     t.Error( "HasClueRefs is not true, but should be" )
   }

   if tt, err := GetText( db, "hum", "kappersklant" ); err != nil {
     t.Error( "GetText failed", err )
   } else {
     if tt.Content != "hum" {
     t.Error( "GetText hum didnt return hum" )
     }
   }

   oneWord, err := regexp.MatchString( "\\w+", "noot" )
   if err != nil {
      if !oneWord {
         t.Error( "MatchString didn't match oneWord in [noot]" )
      }
   }
   oneWord, err = regexp.MatchString( "^\\w+$", "aap noot" )
   if err != nil {
      if oneWord {
         t.Error( "MatchString did match oneWord in [aap noot]" )
      }
   }
}

func getNewPerson( db *pg.DB ) ( persons.Persons, error ) {
   var person persons.Persons
   var err error
   person = persons.MakeANewPerson( db, "kapppersklant" )
   if person.Id < 3 {
     return person, err
   }
   persons.InitCluesForPerson( db, person, "kappersklant" )
   if person, err = persons.RunningPerson( db ); err != nil {
     return person, err
   }
   return person, err
}
