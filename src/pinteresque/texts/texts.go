package texts


import (
    "os"
    "fmt"
    "github.com/go-pg/pg"
    "regexp"
    "log"
)

type Texts struct {
  Id            int
  Direction     string
  Type          string
  Content       string
  Model         string
  Positivematch string
  Negativematch string
  Intent        string
  Cluetoinduce  string
  Active        bool
  Modifier      string
}


func (rcvr *Texts) IsQuestion () bool {
    return rcvr.Type == "?"
}

func TopTexts( db *pg.DB, model string, intent string, personaId int,
               personId int, clue string ) ( []Texts, error ) {
   var texts []Texts
   type parm struct {
       Intent  string
       Model   string
       Persona int
       Person  int
       Clue    string
   }
   log.Printf( "(pinteresque) TopTexts(%d, %d, %s, %s, %s )", personId, personaId, clue, model, intent )
   actual := parm{ Intent: intent,
                    Model: model,
                  Persona: personaId,
                   Person: personId,
                     Clue: clue}
   _, err := db.Query( &texts, `
                  with ttheq as (
                    with theq as (
                    select t.id, t.direction, t.type, t.intent, t.model, t.content,
                           t.positivematch, t.negativematch, t.cluetoinduce,
                           coalesce( (select priority
                                        from clues
                                       where     name = t.cluetoinduce
                                             and person = ?person), 0 ) as p,
                           t.active, t.modifier, 1 as ordering
                      from texts t
                     where     (t.intent ilike ?intent and t.model = ?model)
                           and t.active
                           and coalesce(modifier,'') != '@'
                           and coalesce(modifier,'') != '%'
                           and coalesce(modifier,'') != '~'
                           and t.direction = 'out'
                           and (not
                                (t.id in (select distinct coalesce(pintext,0) from trail
                                          where persona = ?persona and person = ?person)
                                 )
                                or coalesce( t.modifier, '' )
                                   in ('*', '&') )
                           and not
                                (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                                    in (select name from clues
                                         where person = ?person and
                                               (model is null or model = ?model) and
                                               not value is null))
                       UNION
                    select t.id, t.direction, t.type, t.intent, t.model, t.content,
                           t.positivematch, t.negativematch, t.cluetoinduce, 5 as p,
                           t.active, t.modifier, 3 as ordering
                      from texts t
                     where t.intent = (select intent
                                         from texts it
                                        where     it.active
                                              and coalesce( it.modifier, '' ) != '@'
                                              and it.direction = 'in'
                                              and it.model = ?model
                                              and it.cluetoinduce = ?clue
                                        group by intent
                                        order by count( intent ) DESC LIMIT 1)
                           and t.model = ?model
                           and t.active
                           and t.direction = 'out'
                           and (not
                                (t.id in (select distinct coalesce(pintext,0) from trail
                                          where persona = ?persona and person = ?person)
                                 )
                                or coalesce( t.modifier, '' )
                                   in ('*', '&') )
                       UNION
                    select t.id, t.direction, t.type, t.intent, t.model, t.content,
                           t.positivematch, t.negativematch, t.cluetoinduce,
                           coalesce( (select priority
                                        from clues
                                       where     name = t.cluetoinduce
                                             and person = ?person), 0 ) as p,
                           t.active, t.modifier, 4 as ordering
                      from texts t
                     where     (not t.intent ilike ?intent and t.model = ?model)
                           and t.active
                           and coalesce(modifier,'') != '@'
                           and coalesce(modifier,'') != '%'
                           and coalesce(modifier,'') != '~'
                           and t.direction = 'out'
                           and (not
                                (t.id in (select distinct coalesce(pintext,0) from trail
                                          where persona = ?persona and person = ?person)
                                 )
                                or coalesce( t.modifier, '' )
                                   in ('*', '&') )
                           and not
                                (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                                    in (select name from clues
                                         where person = ?person and
                                               (model is null or model = ?model) and
                                               not value is null))
                       UNION
                    select t.id, t.direction, t.type, t.intent, t.model, t.content,
                           t.positivematch, t.negativematch, t.cluetoinduce,
                           coalesce( (select priority
                                        from clues
                                       where     name = t.cluetoinduce
                                             and person = ?person), 0 ) as p,
                           t.active, t.modifier, 5 as ordering
                      from texts t
                     where     (not t.intent ilike ?intent and t.model = ?model)
                           and t.active
                           and coalesce(modifier,'') = '%'
                           and coalesce(modifier,'') != '~'
                           and t.direction = 'out'
                           and (not
                                (t.id in (select distinct coalesce(pintext,0) from trail
                                          where persona = ?persona and person = ?person)
                                 )
                                or coalesce( t.modifier, '' )
                                   in ('%') )
                           and not
                                (replace( replace( cluetoinduce, 'her.', '' ), 'me.', '')
                                    in (select name from clues
                                         where person = ?person and
                                               (model is null or model = ?model) and
                                               not value is null))
                     order by ordering, p
                    limit 10)
                    select id, direction, type, intent, model, content,
                           positivematch, negativematch, cluetoinduce,
                           active, modifier
                      from theq
                     order by ordering, p
                     limit 5)
                   select id, direction, type, intent, model, content,
                          positivematch, negativematch, cluetoinduce,
                          active, modifier
                     from ttheq
                  `, actual )
   return texts, err
}

func GetText( db *pg.DB, content string, model string ) (Texts, error) {
   var thetext Texts
   _, err := db.Query( &thetext, `
                  select *
                    from texts t
                   where     t.content = ?
                         and (t.model is null OR t.model = ?)
                         and t.active
                   order by t.model, random()
                   limit 1
                  `, content, model )
   fmt.Fprintf( os.Stderr, "Returning GetText: %v?\n", thetext )
   return thetext, err
}

func StartText( db *pg.DB, model string ) (Texts, error) {
   var thetext Texts
   _, err := db.Query( &thetext, `
                  select *
                    from texts
                   where     model = ?
                         and active
                         and modifier = '@'
                   order by random()
                   limit 1
                  `, model )
   fmt.Fprintf( os.Stderr, "Returning StartText: %v with error [%v]?\n", thetext, err )
   return thetext, err
}
func GoingAwayText( db *pg.DB, model string ) (Texts, error) {
   var thetext Texts
   _, err := db.Query( &thetext, `
                  select *
                    from texts
                   where     model = ?
                         and active
                         and modifier = '~'
                   order by random()
                   limit 1
                  `, model )
   fmt.Fprintf( os.Stderr, "Returning GoingAwayText: %v with error [%v]?\n", thetext, err )
   return thetext, err
}

  // calling
func (rcvr *Texts) ReduceText( db *pg.DB, personaId int,
                               personId int, model string ) string {
   type parm struct {
       Content  string
       Persona  int
       Person   int
       Model    string
   }
   actual := parm{ Content: rcvr.Content,
                   Persona: personaId,
                    Person: personId,
                     Model: model}
   newtext := rcvr.Content
   for HasClueRefs( newtext ) {
     actual.Content = newtext
     _, err := db.Query( &newtext, `
                  select replace( ?content,
                              (select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[1],
                              case when (select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[2] like 'her.%'
                                   then coalesce((select value
                                           from clues
                                          where name = replace((select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[2],'her.','')
                                                and person = ?person
                                                and model = ?model), (select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[1])
                                   else coalesce((select value
                                           from clues
                                          where name = replace((select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[2],'me.','')
                                                and person = ?persona
                                                and model = ?model), (select regexp_matches(?content, '(\?:(\(((her.|me.|)\w+)\))+){1,1}'))[1])
                               end
                            ) as result
                    limit 1
                  `, actual );
     if err != nil {
       fmt.Fprintf( os.Stderr, "Bailing out in ReduceText, error: %v\n", err )
       return actual.Content
     }
     if actual.Content == newtext { // nothing more to do
       return actual.Content
     }
     actual.Content = newtext
   }
   return actual.Content
}

func HasClueRefs( content string ) bool {
  if matched, err := regexp.MatchString("\\((me.|her.|)[a-z0-9]+\\)", content ); err != nil {
    return false
  } else {
    return matched
  }
}

func TextForSolvingClue( db *pg.DB, clueName string, model string ) ( Texts, error ) {
   var thetext Texts
   _, err := db.Query( &thetext, `
                  select t.id, t.direction, t.type, t.intent, t.model, t.content,
                         t.positivematch, t.negativematch, t.cluetoinduce,
                         t.active, t.modifier
                    from texts t
                   where     t.active
                         and t.direction = 'out'
                         and replace( replace( t.cluetoinduce, 'her.', '' ), 'me.', '') = ?
                         and t.model = ?
                   order by random() limit 1
                  `, clueName, model )
   fmt.Fprintf( os.Stderr, "Returning TextForClue: %v?\n", thetext )
   return thetext, err
}

func Output( gender string, output string ) {
  if output != "" {
    fmt.Printf( gender + " " + output )
    fmt.Printf( "\n" )
  }
}
