package main


import (
 "os"
 "os/exec"
 "fmt"
 "time"
 "log"
 "github.com/davecheney/gpio"
 flg "github.com/jessevdk/go-flags"
)

var options struct {
  Button1   int `short:"b" description:"Number of button 1" default:"17"`
  Button2   int `short:"B" description:"Number of button 2" default:"23"`
  Doreport  bool `short:"d" description:"Call bin/preport at end-command"`
  NoEnding  bool `short:"n" description:"No button will start the end-command"`
  Modelone  string `short:"m" description:"Model to use upon user-action" env:"THISBOT"`
  Modeltwo  string `short:"M" description:"Model to use upon user-reaction" env:"THISBOT"`
  Test      bool `short:"t" description:"Testing: will print out button states every .5 seconds" env:"TESTING"`
  Ring      bool `short:"r" description:"Use ringer to attract attention" env:"RINGER"`
  Verbose   bool `short:"v" description:"Add some messaging to stderr" env:"VERBOSE"`
}

var but1 gpio.Pin // for handset
var but2 gpio.Pin // for handset
var relais gpio.Pin // for relay
var berr error
var moved1 = time.Unix( 0, 0 )
var moved2 = time.Unix( 0, 0 )
var StartRinging *time.Timer
var runningdialogue = false
var ringing = false
func main() {
     _, err := flg.Parse( &options )
     if err != nil {
       log.Printf( "(pin-sidechannel) Error parsing options: %v", err )
       os.Exit( 1 )
     }
     ButInit()
     if options.Ring {
       RelaisInit()
       Ring() // for feedback
     }
     if options.Test {
        if options.Ring {
          relais.Set()
          log.Printf( "Relais Set\n" )
        }
        for {
         if options.Button1 != 0 {
           log.Printf( "But1 (%d): %v", options.Button1, but1.Get() )
         }
         if options.Button2 != 0 {
           log.Printf( "But2 (%d): %v", options.Button2, but2.Get() )
         }
         time.Sleep( 500 * time.Millisecond)
       }
     }
     if options.Button1 != 0 {
       But1handler( HandsetMoves )
     }
     if options.Button2 != 0 {
       But2handler( HandsetMoves )
     }
     if options.Ring {
       StartRinging = time.NewTimer( 20 * time.Minute )
     }
     if options.Ring {
       go func() {
         for {
           _, ok := <- StartRinging.C
           if !ok {
              return
           }
           if !runningdialogue {
               ringing = true
               Ring()
               Ring()
               doDisplay( "Neem op om  te beginnen" )
               if !StartRinging.Stop() {
                  <- StartRinging.C
               }
           }
           ringing = false
           StartRinging.Reset( 20 * time.Minute )
         }
       }()
     }
     for {
        time.Sleep( 1 * time.Second )
     }
}

func HandsetMoves() {
   if time.Now().Sub( moved1 )/time.Millisecond < 500 {
      return
   }
   But1handler( move )
   //But2handler( move )
   time.Sleep( 200 * time.Millisecond )
   moved1 = time.Now()
   if options.Verbose {
     if but1 != nil {
       log.Printf( "Handset b1[%v]\n", but1.Get() )
     }
     if but2 != nil {
       log.Printf( "Handset b2[%v]\n", but2.Get() )
     }
   }
   if but1 != nil && but1.Get() == false && (!runningdialogue || options.NoEnding) {
      doDisplay( "Haak eraf" )
      if !ringing {
        if options.Verbose {
          log.Printf( "Handset picked up, sending [%s%s]\n", "#!model ", options.Modelone )
        }
        fmt.Printf( "#!model " + options.Modelone + "\n")
      } else {
        if options.Verbose {
          log.Printf( "Handset picked up, sending [%s%s]\n", "#!model ", options.Modeltwo )
        }
        fmt.Printf( "#!model " + options.Modeltwo + "\n")
      }
      runningdialogue = true
      if options.Ring {
        StartRinging.Stop()
      }
   }
   if but1 != nil && but1.Get() == false && runningdialogue {
   }
   if but1 != nil && but1.Get() == true && !runningdialogue {
      doDisplay( "Neem op om  te beginnen" )
   }
   if but1 != nil && but1.Get() == true  && runningdialogue {
      doDisplay( "Haak erop" )
      if !options.NoEnding {
        if options.Verbose {
          log.Printf( "Handset put down, sending [%s]\n", "#!end")
        }
        fmt.Printf( "#!end\n")
        runningdialogue = false
        if options.Ring {
          StartRinging.Stop()
          _ = StartRinging.Reset( 20 * time.Minute )
        }
        if options.Doreport {
          log.Printf( "Starting report\n" )
          doReport()
        }
      }
   }
   time.Sleep( 600 * time.Millisecond )
   But1handler( HandsetMoves )
   //But2handler( HandsetMoves )
   moved1 = time.Now()
}

func move() {
   if options.Verbose {
     log.Printf( "Moved\n")
     if but1 != nil {
       log.Printf( "Handset b1[%v]\n", but1.Get() )
     }
     if but2 != nil {
       log.Printf( "Handset b2[%v]\n", but2.Get() )
     }
   }
}

func ButInit() {
  if but1 == nil && options.Button1 != 0 {
     but1, berr  = gpio.OpenPin( options.Button1, gpio.ModeInput )
     but1.BeginWatch( gpio.EdgeFalling, move )
  }
  if but1 == nil && options.Button1 != 0 {
    panic( berr )
  }
  if but2 == nil && options.Button2 != 0 {
     but2, berr = gpio.OpenPin( options.Button2, gpio.ModeInput )
     but2.BeginWatch( gpio.EdgeFalling, move )
  }
  if but2 == nil && options.Button2 != 0 {
    panic( berr )
  }
}

func RelaisInit() {
  if relais == nil {
     relais, berr  = gpio.OpenPin( 12, gpio.ModeOutput )
  }
  if relais == nil {
    panic( berr )
  }
}

func But1handler( handlerUp gpio.IRQEvent ) {
     if handlerUp == nil {
       but1.BeginWatch( gpio.EdgeBoth, move )
     } else {
       but1.BeginWatch( gpio.EdgeRising, handlerUp )
     }
}

func But2handler( handlerUp gpio.IRQEvent ) {
     if handlerUp == nil {
       but2.BeginWatch( gpio.EdgeBoth, move )
     } else {
       but2.BeginWatch( gpio.EdgeRising, handlerUp )
     }
}

func Ring() { // gpio 12, set 12 to high for ON
  for n:= 3; n >= 0; n -= 1 {
    if !runningdialogue {
      relais.Set()
      log.Printf( "Ringing\n" )
      time.Sleep( 200 * time.Millisecond )
      relais.Clear()
      if runningdialogue {
         break
      }
      log.Printf( "Silent\n" )
      time.Sleep( 100 * time.Millisecond )
    }
    ringing = false
  }
}

func doDisplay( s string ) {
   cmd := exec.Command( "bin/todisplay", s )
   if err := cmd.Run(); err != nil {
      log.Printf( "Todisplay failed [%v]\n", err )
   }
}

func doReport() {
   log.Printf( "Doing report now\n" )
   cmd := exec.Command( "bin/preport" )
   if err := cmd.Run(); err != nil {
      log.Printf( "Reporting failed [%v]\n", err )
   }
}
