package main


import (
   "os"
   "io"
   "fmt"
   "time"
)

var textChannel = make( chan string, 1 )
var skip bool

func main() {
   skip = false
   go readIt()
   for true {
      theLine := <-textChannel
      skip = true
      switch theLine {
        case "stop\n":
          os.Exit(0)
        default:
          os.Stdout.Write( []byte( "Read: [" + theLine + "]" ) )
      }
      os.Stdout.Write( []byte( "skipping\n" ) )
      count := 5000
      for count > 0 {
         count -= 1
         time.Sleep( 1 * time.Millisecond )
      }
      skip = false
      os.Stdout.Write( []byte( "allowing\n" ) )
   }
}

 func readIt() {
   for true {
      if theLine, err := readLine(); err != nil {
         os.Exit(1)
      } else {
        if !skip {
           textChannel <- theLine
        } else {
           fmt.Fprintf( os.Stderr, "skipped %s", theLine )
        }
      }
   }
 }

 func readLine() (string, error) {
   var result []byte
   terminator := byte( 0xa )
   buf := make( []byte, 1 )
   for buf[ 0 ] != terminator {
      if _, err := os.Stdin.Read( buf ); err != nil {
         if err != io.EOF {
            panic( err )
         }
         return "", err
      }
      result = append( result, buf[ 0 ] )
   }
   return string( result ), nil
 }
