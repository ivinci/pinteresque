package main


import (
    "context"
    "os"
    "log"
    "strings"
    "errors"
    texttospeech "cloud.google.com/go/texttospeech/apiv1"
    texttospeechpb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

func main() {
    ctx := context.Background()

    client, err := texttospeech.NewClient(ctx)
    if err != nil {
        log.Fatal(err)
    }

    encodingname := "LINEAR16"
    sampleratename := "16k"
    gendersign := "f"
    text := ""
    voicename := "nl-NL-Wavenet-A"

    if len( os.Args ) == 5 {
      encodingname = os.Args[ 1 ]
      sampleratename = os.Args[ 2 ]
      gendersign = strings.ToLower( os.Args[ 3 ] )
      text = os.Args[ 4 ]
    }
    if len( os.Args ) == 4 {
      encodingname = os.Args[ 1 ]
      gendersign = strings.ToLower( os.Args[ 2 ] )
      text = os.Args[ 3 ]
    }
    if len( os.Args ) == 3 {
      gendersign = strings.ToLower( os.Args[ 1 ] )
      text = os.Args[ 2 ]
    }
    if len( os.Args ) == 2 {
      text = os.Args[ 1 ]
    }

    gender := texttospeechpb.SsmlVoiceGender_FEMALE
    switch gendersign {
     case "m", "♂", "":
       gender = texttospeechpb.SsmlVoiceGender_MALE
     case "v", "♀", "f":
       gender = texttospeechpb.SsmlVoiceGender_FEMALE
     default: // no gender sign, we'll just speak it
       if len( os.Args ) == 3 {
         text = os.Args[ 1 ] + " " + os.Args[ 2 ]
       }
    }
    audioencoding := texttospeechpb.AudioEncoding_LINEAR16
    switch encodingname { // see https://cloud.google.com/speech-to-text/docs/encoding
     case "MP3":
          audioencoding = texttospeechpb.AudioEncoding_MP3
     case "LINEAR16":
          audioencoding = texttospeechpb.AudioEncoding_LINEAR16
     case "OGG_OPUS":
          audioencoding = texttospeechpb.AudioEncoding_OGG_OPUS
     default:
        log.Fatal( errors.New( "Unsupported audioencoding selected" ) )
    }
    var samplerate int32
    samplerate = 16000
    switch sampleratename { // see https://cloud.google.com/speech-to-text/docs/encoding
     case "8k", "8000":
          samplerate = 8000
     case "16k", "16000":
          samplerate = 16000
     case "32k", "32000":
          samplerate = 32000
     case "48k", "48000":
          samplerate = 48000
     default:
        log.Fatal( errors.New( "Unsupported samplerate selected" ) )
    }
    // somehow, the google text-to-speech API doesnt need the samplerate
    if samplerate < 0 {
        log.Printf( "Samplerate is %d, but is not used by the API\n", samplerate )
    }

    var req texttospeechpb.SynthesizeSpeechRequest
    // very strange, if i set Name in voiceselectionparams, the male voice cannot be used, hence the following trick
    if gender == texttospeechpb.SsmlVoiceGender_MALE {
      req = texttospeechpb.SynthesizeSpeechRequest{
          Input: &texttospeechpb.SynthesisInput{
              InputSource: &texttospeechpb.SynthesisInput_Text{Text: text },
          },
          Voice: &texttospeechpb.VoiceSelectionParams{
              LanguageCode: "nl-NL",
              SsmlGender:   gender,
          },
          AudioConfig: &texttospeechpb.AudioConfig{
              AudioEncoding: audioencoding,
          },
      }
    } else {
      req = texttospeechpb.SynthesizeSpeechRequest{
          Input: &texttospeechpb.SynthesisInput{
              InputSource: &texttospeechpb.SynthesisInput_Text{Text: text },
          },
          Voice: &texttospeechpb.VoiceSelectionParams{
              LanguageCode: "nl-NL",
              Name: voicename,
              SsmlGender:   gender,
          },
          AudioConfig: &texttospeechpb.AudioConfig{
              AudioEncoding: audioencoding,
          },
      }
    }

    resp, err := client.SynthesizeSpeech(ctx, &req)
    if err != nil {
        log.Fatal(err)
    }

    os.Stdout.Write( resp.AudioContent )
}
