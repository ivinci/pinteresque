package main


import (
 "os"
 "os/exec"
 "fmt"
 "regexp"
 "bufio"
 "time"
 "log"
 flg "github.com/jessevdk/go-flags"
)

var options struct {
  Button1   string `short:"b" description:"Name of button 1" default:"KEY_LEFT"`
  Button2   string `short:"B" description:"Name of button 2" default:"KEY_RIGHT"`
  Button3   string `short:"c" description:"Name of button 3" default:"KEY_NONE"`
  Doreport  bool   `short:"d" description:"Call bin/preport at end-command"`
  Modelone  string `short:"m" description:"Model to use upon user-action" env:"THISBOT"`
  Verbose   bool   `short:"v" description:"Add some messaging to stderr" env:"VERBOSE"`
}

var moved1 = time.Unix( 0, 0 )
var moved2 = time.Unix( 0, 0 )
var moved3 = time.Unix( 0, 0 )
var moved4 = time.Unix( 0, 0 )
var moved5 = time.Unix( 0, 0 )
var runningdialogue = false
func main() {
  _, err := flg.Parse( &options )
  if err != nil {
    log.Printf( "(mouse-sidechannel) Error parsing options: %v", err )
    os.Exit( 1 )
  }
  ButInit()
}

func StartBot() {
   if time.Now().Sub( moved1 )/time.Millisecond < 500 {
      return
   }
   time.Sleep( 200 * time.Millisecond )
   moved1 = time.Now()
   if options.Verbose {
     log.Printf( "Handset picked up, sending [%s%s]\n", "#!model ", options.Modelone )
   }
   fmt.Printf( "#!model " + options.Modelone + "\n")
   time.Sleep( 600 * time.Millisecond )
   moved1 = time.Now()
}
func StartEndTune() {
   if time.Now().Sub( moved2 )/time.Millisecond < 500 {
      return
   }
   if options.Modelone == "sorry" {
     cmd := exec.Command( "bin/nocap" )
     if err := cmd.Run(); err != nil {
       log.Printf( "(mouse-sidechannel) nocap failed [%v]\n", err )
     }
   }
   if options.Verbose {
     log.Printf( "Handset put down, sending [%s]\n", "#!end")
   }
   fmt.Printf( "#!end\n")
   time.Sleep( 600 * time.Millisecond )
   moved2 = time.Now()
}

func doReport() {
   if time.Now().Sub( moved3 )/time.Millisecond < 500 {
      return
   }
   time.Sleep( 200 * time.Millisecond )
   moved3 = time.Now()
   if options.Verbose {
     log.Printf( "Doing report now\n" )
   }
   cmd := exec.Command( "bin/preport" )
   if err := cmd.Run(); err != nil {
      log.Printf( "Reporting failed [%v]\n", err )
   }
   time.Sleep( 600 * time.Millisecond )
   moved3 = time.Now()
}
func doPowerdown() {
   if time.Now().Sub( moved4 )/time.Millisecond < 500 {
      return
   }
   moved4 = time.Now()
   log.Printf( "Doing shutdown now\n" )
   cmd := exec.Command( "sudo", "shutdown", "-P", "now" )
   if err := cmd.Run(); err != nil {
      log.Printf( "Shutting down failed [%v]\n", err )
   }
   // notreached…
   time.Sleep( 600 * time.Millisecond )
   moved4 = time.Now()
}
func doRestart() {
   if time.Now().Sub( moved5 )/time.Millisecond < 500 {
      return
   }
   moved5 = time.Now()
   log.Printf( "Doing restart pinteresque now\n" )
   cmd := exec.Command( "sudo", "systemctl", "restart", "pinteresque" )
   if err := cmd.Run(); err != nil {
      log.Printf( "Restarting Pinteresque failed [%v]\n", err )
   }
   // notreached…
   time.Sleep( 600 * time.Millisecond )
   moved5 = time.Now()
}

// match two things from "Event: time 1610445067.890861, type 1 (EV_KEY), code 105 (KEY_LEFT), value 0"
// key and value
var mouseline = regexp.MustCompile( "Event:.*, type . .(EV_[A-Z]*)., code [0-9]* .(KEY_[A-Z]*)., value ([0-9a-f]*)")

func ButInit() {
  r := bufio.NewReader( os.Stdin )
  for s, err := r.ReadString( '\n' );
     err == nil;
     s, err = r.ReadString( '\n' ) {
    if options.Verbose {
      log.Printf( "(mouse-sidechannel) on input: %s", s )
    }
    if len( s ) <= 6 {
      log.Printf( "(mouse-sidechannel) reading a line too short %s, ignoring it", s )
      continue
    }
    matches := mouseline.FindStringSubmatch( s )
    if matches == nil || len( matches ) != 4 {
      if options.Verbose {
        log.Printf( "(mouse-sidechannel) no match reading line %s, ignoring it", s )
      }
      continue
    }
    inputtype  := matches[ 1 ]
    keyname    := matches[ 2 ]
    inputvalue := matches[ 3 ]
    if options.Verbose {
      log.Printf( "(mouse-sidechannel) mouse has ([%s],[%s],[%s])", inputtype, keyname, inputvalue )
    }
     // only go to work for inputvalue 0, which is button-release.
    switch keyname {
      case options.Button1:
	if inputvalue == "0" {
          StartBot()
        }
      case options.Button2:
	if inputvalue == "0" {
          StartEndTune()
        }
      case options.Button3:
	if inputvalue == "0" {
          doReport()
        }
      case "KEY_POWER":
        if inputvalue == "0" {
          doPowerdown()
        }
      case "KEY_HOMEPAGE":
        if inputvalue == "0" {
          doRestart()
        }
      default:
        log.Printf( "(mouse-sidechannel) (%s,%s,%s) not handled", inputtype, keyname, inputvalue )
    }
  }
  if options.Verbose {
    log.Printf( "(mouse-sidechannel) finished input" )
  }
}
