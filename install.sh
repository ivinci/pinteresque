#!/bin/sh
apt update
apt upgrade

for serv in analytics audio-server snips-hotword skill-server asr dialogue nlu tts injection
do
  systemctl disable snips-${serv}.service
  systemctl stop snips-${serv}.service
done

apt install postgresql
echo "configure postgres-server to trust all incoming logons first"; read answ
su -c "createdb pin" postgres
su -c "createuser joost" postgres
su -c "psql -h localhost -d pin -f create.ddl" pi
su -c "psql -h localhost -d pin -f inserts.ddl" pi

apt install csvtool
apt install gawk
apt install rsync
apt install net-tools
apt install openssh
apt install autossh
apt install sox

apt install python3-scipy
apt install python3-openturns
apt install libopenblas-dev
apt install python3-ripe-atlas-sagan
apt install python3-sklearn-lib python3-sklearn-pandas
apt install python3-pip
pip3 install rasa_nlu
pip3 install rasa_nlu[spacy]
python3 -m spacy download nl_core_news_sm
python3 -m spacy download nl

apt install frog frogdata

git clone https://github.com/opensource-spraakherkenning-nl/Kaldi_NL.git
git clone https://github.com/kaldi-asr/kaldi.git kaldi --origin upstream

sox --help | grep 'FORMAT.*mp3' || apt install libsox-fmt-mp3
