#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h>

hd44780_I2Cexp lcd; //define lcd

const int LCD_COLS = 13; // amount of character per row
const int LCD_ROWS = 2; // amount of rows

#include <RBDdimmer.h>//

#define USE_SERIAL  Serial
#define outputPin  3  //
#define zerocross  2  //z-c to pin 2 of arduino

dimmerLamp dimmer(outputPin);

String a;

void setup() {
  Serial.begin(9600); //start listening on port 9600

  lcd.begin(LCD_COLS, LCD_ROWS); //start lcd
  lcd.print("pick up"); //welcome message
  lcd.setCursor(0,1);
  lcd.print("to start");

  USE_SERIAL.begin(9600);
  dimmer.begin(NORMAL_MODE, ON); //start ac dimmer bel
}

void loop()
{

  if (Serial.available() > 0) {
    a= Serial.readString();// read the incoming data from rpi
    String first = a.substring(0,12); //split up data for lcd row 1
    String last = a.substring(12,25); //split up data for lcd row 2
    lcd.clear(); //clear lcd when serial message is received
    lcd.setCursor(0,0);
    lcd.print(first); //print first row to lcd
    lcd.setCursor(0,1);
    lcd.print(last); //print second row to lcd
    delay(100);
  }
  dimmer.setPower(40); //bel percentage
}
